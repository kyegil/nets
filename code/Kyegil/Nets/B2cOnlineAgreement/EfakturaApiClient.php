<?php

namespace Kyegil\Nets\B2cOnlineAgreement;

use DateTimeInterface;
use Exception;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\ConsignmentStatusResponse;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\EfakturaIdentifierKey;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\EfakturaIdentifierResponse;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\InvoiceStatusResponse;
use Kyegil\Nets\Exceptions\ApiFault;
use Kyegil\Nets\Exceptions\ServerFault;
use Kyegil\Nets\Exceptions\ValidationFault;
use SoapFault;

/**
 *
 */
class EfakturaApiClient
{
    /**
     * @var string
     */
    protected string $brukernavn;
    /**
     * @var string
     */
    protected string $passord;
    /**
     * @var string
     */
    protected string $url;
    /**
     * @var SoapClient
     */
    protected SoapClient $soapClient;

    /**
     * @param string $brukernavn
     * @param string $passord
     * @param string $url The NETS Soap endpounit for testing or production (without ?wsdl ending)
     * @param string|null $sertifikatFil Filbane for sertifikat utstect av NETS for bruk ved HTTPS autentisering
     * @param string|null $sertifikatPrivateKey Filbane for private key tilhørende sertifikatet
     * @param string|null $sertifikatPKPassPhrase Passord for private key
     * @param array $soapOptions
     * @throws \SoapFault
     */
    public function __construct(
        string  $brukernavn = '',
        string  $passord = '',
        string  $url = 'https://payments.mastercard.no/efaktura/b2c-online-agreement/v3/',
        ?string $sertifikatFil = null,
        ?string $sertifikatPrivateKey = null,
        ?string $sertifikatPKPassPhrase = null,
        array   $soapOptions = []
    )
    {
        $this->brukernavn = $brukernavn;
        $this->passord = $passord;
        $this->url = $url;
        $wsdl = $url . '?wsdl';

        $options = [
            'location' => $url,
            'login' => $this->brukernavn,
            'password' => $this->passord,
            'local_cert' => $sertifikatFil,
            'passphrase' => $sertifikatPKPassPhrase,
            'trace' => true,
            'connection_timeout' => 5,
            'stream_context' => stream_context_create([]),
            'features' => 1, // 1 = SOAP_SINGLE_ELEMENT_ARRAYS
        ];

        $soapOptions = array_merge($options, $soapOptions);

        /** @var resource $streamContext */
        $streamContext = $soapOptions['stream_context'];

        if($sertifikatPrivateKey) {
            unset($options['local_cert'], $options['passphrase']);
            stream_context_set_option($streamContext, 'ssl', 'local_cert', $sertifikatFil);
            stream_context_set_option($streamContext, 'ssl', 'passphrase', $sertifikatPKPassPhrase);
            stream_context_set_option($streamContext, 'ssl', 'local_pk', $sertifikatPrivateKey);
        }

        $originalConnectionTimeout = ini_get('default_socket_timeout');
        if (isset($soapOptions['connection_timeout'])) {
            ini_set('default_socket_timeout', $soapOptions['connection_timeout']);
        }
        $this->soapClient = new SoapClient($wsdl, $soapOptions);
        ini_set('default_socket_timeout', $originalConnectionTimeout);
        $this->generateHeader();
    }

    /**
     * @return SoapClient
     */
    public function getSoapClient(): SoapClient
    {
        return $this->soapClient;
    }

    /**
     * @param SoapClient $soapClient
     * @return $this
     */
    public function setSoapClient(SoapClient $soapClient): EfakturaApiClient
    {
        $this->soapClient = $soapClient;
        return $this;
    }

    /**
     * @param EfakturaIdentifierKey[] $efakturaIdentifierKeys
     * @param string|null $messageIdentifier
     * @return EfakturaIdentifierResponse[]
     * @throws Exception
     */
    public function getEfakturaIdentifiers(array $efakturaIdentifierKeys, ?string $messageIdentifier = null): array
    {
        /** @var EfakturaIdentifierResponse[] $responses */
        $responses = [];
        /** @var string $messageIdentifier */
        $messageIdentifier = $messageIdentifier ?? date('c-') . substr(md5(rand()), 0, 6);

        $getEfakturaIdentifiers = (object)[
            'auditInformation' => (object)[
                'messageIdentifier' => $messageIdentifier
            ],
            'getEfakturaIdentifiersKey' => (object)[
                'efakturaIdentifierKey' => []
            ],
            'returnMatchingKey' => true,
        ];
        foreach ($efakturaIdentifierKeys as $efakturaIdentifierKey) {
            $getEfakturaIdentifiers->getEfakturaIdentifiersKey->efakturaIdentifierKey[]
                = $efakturaIdentifierKey->getData();
        }

        /**
         * @var object $soapResponse
         * @link https://www.efaktura.no/developer/documentation/issuer-via-mastercard-payment-services-service#op.N66749
         */
        $soapResponse = $this->soapClient->getEfakturaIdentifiers($getEfakturaIdentifiers);
        if (($soapResponse->messageIdentifier ?? '') != $messageIdentifier) {
            throw new Exception('Non Matching message identifier');
        }
        foreach ($soapResponse->getEfakturaIdentifiersResponse->efakturaIdentifierResponse ?? [] as $efakturaIdentifierResponse) {
            $responses[] = new EfakturaIdentifierResponse($efakturaIdentifierResponse);
        }
        return $responses;
    }

    /**
     * Generate Header
     *
     * @return EfakturaApiClient
     */
    private function generateHeader(): EfakturaApiClient
    {
        $headerXml = new \SoapVar('
<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <wsse:UsernameToken>
        <wsse:Username>' . $this->brukernavn . '</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' . $this->passord . '</wsse:Password>
    </wsse:UsernameToken>
</wsse:Security>
', XSD_ANYXML);

        $security = new \SoapHeader(
            'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
            'Security',
            $headerXml,
            false
        );
        $this->soapClient->__setSoapHeaders($security);
        return $this;
    }
    /**
     * @param object[] $invoices
     * @param string $consignmentId
     * @param string|null $messageIdentifier
     * @return EfakturaApiClient
     * @throws ApiFault
     * @throws ServerFault
     * @throws ValidationFault
     * @throws SoapFault
     */
    public function addInvoices(array $invoices, string $consignmentId, ?string $messageIdentifier = null): EfakturaApiClient
    {
        /** @var string $messageIdentifier */
        $messageIdentifier = $messageIdentifier ?? date('c-') . substr(md5(rand()), 0, 6);

        $addInvoices = (object)[
            'auditInformation' => (object)[
                'messageIdentifier' => $messageIdentifier
            ],
            'consignmentId' => $consignmentId,
            'invoices' => (object)['invoice' => $invoices],
        ];

        /**
         * @var object $soapResponse
         * @link https://www.efaktura.no/developer/documentation/issuer-via-mastercard-payment-services-service#op.N66735
         */
        $soapResponse = $this->soapClient->addInvoices($addInvoices);
        if (isset($soapResponse->errorCode)) {
            throw new ValidationFault($soapResponse);
        }
        if (!isset($soapResponse->noOfInvoices)) {
            throw new ServerFault($soapResponse);
        }
        if (($soapResponse->messageIdentifier ?? '') != $messageIdentifier) {
            throw new ApiFault('Non Matching message identifier');
        }
        return $this;
    }

    /**
     * @param int $invoiceId
     * @param string|null $messageIdentifier
     * @return EfakturaApiClient
     * @throws ApiFault
     * @throws ServerFault
     * @throws ValidationFault
     */
    public function cancelInvoice(int $invoiceId, ?string $messageIdentifier = null): EfakturaApiClient
    {
        /** @var string $messageIdentifier */
        $messageIdentifier = $messageIdentifier ?? date('c-') . substr(md5(rand()), 0, 6);

        $cancelInvoice = (object)[
            'auditInformation' => (object)[
                'messageIdentifier' => $messageIdentifier
            ],
            'invoiceId' => $invoiceId,
        ];

        /**
         * @var object $soapResponse
         * @link https://www.efaktura.no/developer/documentation/issuer-via-mastercard-payment-services-service#op.N66777
         */
        $soapResponse = $this->soapClient->cancelInvoice($cancelInvoice);
        if (isset($soapResponse->errorCode)) {
            throw new ValidationFault($soapResponse);
        }
        if (!isset($soapResponse->invoiceId)) {
            throw new ServerFault($soapResponse);
        }
        if ($soapResponse->invoiceId != $cancelInvoice->invoiceId) {
            throw new ApiFault('Non Matching invoice id');
        }
        return $this;
    }

    /**
     * Get Cancellable Invoice
     *
     * Find cancellable invoice using mandatory and optional parameters
     *
     * Possible validation error codes from operation
     * 5004 - Unable to find invoice
     * 5005 - Cancellation on paid invoice
     * 5006 - Invoice already cancelled
     * 5007 - Multiple invoices found
     *
     * @param string $issuer
     * @param DateTimeInterface $dueDate
     * @param float $amount
     * @param string $documentType
     * @param string $accountNumber
     * @param string|null $kid
     * @param string|null $messageIdentifier
     * @return int InvoiceId
     * @throws ApiFault
     * @throws ServerFault
     * @throws ValidationFault
     */
    public function getCancellableInvoice(
        string $issuer,
        DateTimeInterface $dueDate,
        float $amount,
        string $documentType,
        string $accountNumber,
        ?string $kid = null,
        ?string $messageIdentifier = null
    ): int
    {
        if($dueDate instanceof \DateTime) {
            $dueDate = \DateTimeImmutable::createFromMutable($dueDate);
        }
        $dueDate = $dueDate->setTimezone(new \DateTimeZone('UTC'));
        /** @var string $messageIdentifier */
        $messageIdentifier = $messageIdentifier ?? date('c-') . substr(md5(rand()), 0, 6);

        $cancellableInvoice = (object)[
            'auditInformation' => (object)[
                'messageIdentifier' => $messageIdentifier
            ],
            'issuer' => $issuer,
            'dueDate' => $dueDate->format('Y-m-d'),
            'amount' => $amount,
            'accountNumber' => $accountNumber,
            'documentType' => $documentType,
        ];
        if($kid) {
            $cancellableInvoice->kid = $kid;
        }

        /**
         * @var object $soapResponse
         * @link https://www.efaktura.no/developer/documentation/issuer-via-mastercard-payment-services-service#op.N66763
         */
        $soapResponse = $this->soapClient->getCancellableInvoice($cancellableInvoice);
        if (isset($soapResponse->errorCode)) {
            throw new ValidationFault($soapResponse);
        }
        if (!isset($soapResponse->invoiceId)) {
            throw new ServerFault($soapResponse);
        }
        if (($soapResponse->messageIdentifier ?? '') != $messageIdentifier) {
            throw new ApiFault('Non Matching message identifier');
        }
        return $soapResponse->invoiceId;
    }

    /**
     * @param string $consignmentId
     * @param string|null $messageIdentifier
     * @return ConsignmentStatusResponse
     * @throws ApiFault
     * @throws ServerFault
     * @throws ValidationFault
     */
    public function getConsignmentStatus(string $consignmentId, ?string $messageIdentifier = null): ConsignmentStatusResponse
    {
        /** @var string $messageIdentifier */
        $messageIdentifier = $messageIdentifier ?? date('c-') . substr(md5(rand()), 0, 6);

        $getConsignmentStatus = (object)[
            'auditInformation' => (object)[
                'messageIdentifier' => $messageIdentifier
            ],
            'consignmentId' => $consignmentId,
        ];

        /**
         * @var object $soapResponse
         * @link https://www.efaktura.no/developer/documentation/issuer-via-mastercard-payment-services-service#op.N66707
         */
        $soapResponse = $this->soapClient->getConsignmentStatus($getConsignmentStatus);
        if (isset($soapResponse->errorCode)) {
            throw new ValidationFault($soapResponse);
        }
        if (!isset($soapResponse->status)) {
            throw new ServerFault($soapResponse);
        }
        return new ConsignmentStatusResponse($soapResponse);
    }

    /**
     * @param string $issuer
     * @param string[] $invoiceNumbers
     * @param bool $showDistributed
     * @param string|null $messageIdentifier
     * @return InvoiceStatusResponse[]
     * @throws ApiFault
     * @throws ServerFault
     * @throws ValidationFault
     */
    public function getInvoiceStatus(
        string $issuer,
        array $invoiceNumbers,
        bool $showDistributed = false,
        ?string $messageIdentifier = null
    ): array
    {
        /** @var string $messageIdentifier */
        $messageIdentifier = $messageIdentifier ?? date('c-') . substr(md5(rand()), 0, 6);

        $invoiceStatus = (object)[
            'auditInformation' => (object)[
                'messageIdentifier' => $messageIdentifier
            ],
            'getInvoiceStatuses' => (object)[
                'showDistributed' => $showDistributed,
                'getInvoiceStatus' => []
            ],
        ];

        foreach ($invoiceNumbers as $invoiceNumber) {
            $invoiceStatus->getInvoiceStatuses->getInvoiceStatus[] = (object)[
                'invoiceNumber' => $invoiceNumber,
                'issuer' => $issuer,
            ];
        }

        /**
         * @var object $soapResponse
         * @link https://www.efaktura.no/developer/documentation/issuer-via-mastercard-payment-services-service#op.N66721
         */
        $soapResponse = $this->soapClient->getInvoiceStatus($invoiceStatus);
        if (isset($soapResponse->errorCode)) {
            throw new ValidationFault($soapResponse);
        }
        if (!isset($soapResponse->invoiceStatusResponses->invoiceStatusResponse)) {
            throw new ServerFault($soapResponse);
        }

        $invoiceStatusResponses = [];
        foreach ($soapResponse->invoiceStatusResponses->invoiceStatusResponse as $invoiceStatusResponse) {
            $invoiceStatusResponses[] = new InvoiceStatusResponse($invoiceStatusResponse);
        }
        return $invoiceStatusResponses;
    }
}