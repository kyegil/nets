<?php

namespace Kyegil\Nets\B2cOnlineAgreement;

/**
 *
 * @method object addInvoices(object $addInvoices)
 * @method object cancelInvoice(object $cancelInvoice)
 * @method object getCancellableInvoice(object $cancellableInvoice)
 * @method object getConsignmentStatus(object $getConsignmentStatus)
 * @method object getEfakturaIdentifiers(object $getEfakturaIdentifiers)
 * @method object getInvoiceStatus(object $getInvoiceStatus)
 * @link https://www.efaktura.no/developer/documentation/submitter-service
 */
class SoapClient extends \SoapClient
{
}