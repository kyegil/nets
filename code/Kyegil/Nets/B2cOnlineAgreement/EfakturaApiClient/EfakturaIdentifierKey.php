<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient;

use DateTimeInterface;

/**
 *
 */
class EfakturaIdentifierKey
{
    /** @var array */
    protected array $data;

    /**
     * @var string
     */
    protected string $countryPhonePrefix = '+47';

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getData());
    }

    /**
     * @param object|array $data
     * @return EfakturaIdentifierKey
     */
    public function setData($data): EfakturaIdentifierKey
    {
        $this->data = json_decode(json_encode($data), true);
        return $this;
    }

    /**
     * @param string $prefix
     * @return EfakturaIdentifierKey
     */
    public function setCountryPhonePrefix(string $prefix): EfakturaIdentifierKey
    {
        $this->countryPhonePrefix = $prefix;
        return $this;
    }

    /**
     * @param string|null $id
     * @return EfakturaIdentifierKey
     */
    public function setId(?string $id): EfakturaIdentifierKey
    {
        $id = $id
            ? filter_var($id, FILTER_SANITIZE_NUMBER_INT)
            : null;
        $this->data['id'] = $id;
        return $this;
    }

    /**
     * @param string|null $efakturaIdentifier
     * @return EfakturaIdentifierKey
     */
    public function setEfakturaIdentifier(?string $efakturaIdentifier): EfakturaIdentifierKey
    {
        if($efakturaIdentifier) {
            $efakturaIdentifier = trim($efakturaIdentifier);
            $efakturaIdentifier = preg_match('/^[0-9]{9}$/', $efakturaIdentifier) ? $efakturaIdentifier : null;
        }
        $this->data['efakturaIdentifier'] = $efakturaIdentifier;
        return $this;
    }

    /**
     * @param string|null $socialSecurityNumber
     * @return EfakturaIdentifierKey
     */
    public function setSocialSecurityNumber(?string $socialSecurityNumber): EfakturaIdentifierKey
    {
        if ($socialSecurityNumber) {
            $socialSecurityNumber = filter_var($socialSecurityNumber, FILTER_SANITIZE_NUMBER_INT);
        }
        $this->data['socialSecurityNumber'] =
            strlen($socialSecurityNumber) == 11
            ? $socialSecurityNumber
                : null;
        return $this;
    }

    /**
     * @param string|null $organizationNumber
     * @return EfakturaIdentifierKey
     */
    public function setOrganizationNumber(?string $organizationNumber): EfakturaIdentifierKey
    {
        if ($organizationNumber) {
            $organizationNumber = filter_var($organizationNumber, FILTER_SANITIZE_NUMBER_INT);
            $organizationNumber = strlen($organizationNumber) == 9
                ? $organizationNumber
                : null;
        }
        $this->data['organizationNumber'] = $organizationNumber;
        return $this;
    }

    /**
     * @param string|null $telephoneAlias
     * @return EfakturaIdentifierKey
     */
    public function setTelephoneAlias(?string $telephoneAlias): EfakturaIdentifierKey
    {
        if(isset($telephoneAlias)) {
            $telephoneAlias = trim($telephoneAlias);
            if(substr($telephoneAlias, 0, 2) === '00') {
                $telephoneAlias = '+' . substr($telephoneAlias, 2);
            }
            $telephoneAlias = ltrim($telephoneAlias, '0');
            if(substr($telephoneAlias, 0, 1) !== '+') {
                $telephoneAlias = $this->countryPhonePrefix . $telephoneAlias;
            }
            $telephoneAlias = preg_match('/^\+[1-9][0-9]{4,14}$/', $telephoneAlias) ? $telephoneAlias : null;
        }
        $this->data['telephoneAlias'] = $telephoneAlias;
        return $this;
    }

    /**
     * @param string|null $emailAlias
     * @return EfakturaIdentifierKey
     */
    public function setEmailAlias(?string $emailAlias): EfakturaIdentifierKey
    {
        if(isset($emailAlias)) {
            $emailAlias = filter_var($emailAlias, FILTER_SANITIZE_EMAIL);
            $emailAlias = mb_strlen($emailAlias) >= 5 && mb_strlen($emailAlias) <= 100 ? $emailAlias : null;
        }
        $this->data['emailAlias'] = $emailAlias;
        return $this;
    }

    /**
     * @param string|null $firstName
     * @return EfakturaIdentifierKey
     */
    public function setFirstName(?string $firstName): EfakturaIdentifierKey
    {
        if(isset($firstName)) {
            $firstName = mb_substr(trim($firstName), 0, 50);
        }
        settype($this->data['name'], 'array');
        $this->data['name']['firstName'] = $firstName;
        return $this;
    }

    /**
     * @param string|null $lastName
     * @return EfakturaIdentifierKey
     */
    public function setLastName(?string $lastName): EfakturaIdentifierKey
    {
        if(isset($lastName)) {
            $lastName = mb_substr(trim($lastName), 0, 50);
        }
        settype($this->data['name'], 'array');
        $this->data['name']['lastName'] = $lastName;
        return $this;
    }

    /**
     * @param string|null $addressLine1
     * @return EfakturaIdentifierKey
     */
    public function setAddressLine1(?string $addressLine1): EfakturaIdentifierKey
    {
        if(isset($addressLine1)) {
            $addressLine1 = mb_substr(trim($addressLine1), 0, 50);
        }
        settype($this->data['address'], 'array');
        $this->data['address']['addressLine1'] = $addressLine1;
        return $this;
    }

    /**
     * @param string|null $addressLine2
     * @return EfakturaIdentifierKey
     */
    public function setAddressLine2(?string $addressLine2): EfakturaIdentifierKey
    {
        if(isset($addressLine2)) {
            $addressLine2 = mb_substr(trim($addressLine2), 0, 50);
        }
        settype($this->data['address'], 'array');
        $this->data['address']['addressLine2'] = $addressLine2;
        return $this;
    }

    /**
     * @param string|null $postalCode
     * @return EfakturaIdentifierKey
     */
    public function setPostalCode(?string $postalCode): EfakturaIdentifierKey
    {
        if(isset($postalCode)) {
            $postalCode = trim($postalCode);
            $postalCode = mb_strlen($postalCode) >= 4 && mb_strlen($postalCode) <= 10 ? $postalCode : null;
        }
        settype($this->data['address'], 'array');
        $this->data['address']['postalCode'] = $postalCode;
        return $this;
    }

    /**
     * @param string|null $city
     * @return EfakturaIdentifierKey
     */
    public function setCity(?string $city): EfakturaIdentifierKey
    {
        if(isset($city)) {
            $city = trim($city);
            $city = mb_strlen($city) <= 30 ? $city : null;
        }
        settype($this->data['address'], 'array');
        $this->data['address']['city'] = $city;
        return $this;
    }

    /**
     * @param string|null $countryCode
     * @return EfakturaIdentifierKey
     */
    public function setCountryCode(?string $countryCode): EfakturaIdentifierKey
    {
        if(isset($countryCode)) {
            $countryCode = trim($countryCode);
            $countryCode = mb_strlen($countryCode) == 2 ? $countryCode : null;
        }
        settype($this->data['address'], 'array');
        $this->data['address']['countryCode'] = $countryCode;
        return $this;
    }

    /**
     * @param string|null $organizationNumber
     * @return EfakturaIdentifierKey
     */
    public function setInvoiceIssuer(?string $organizationNumber): EfakturaIdentifierKey
    {
        if ($organizationNumber) {
            $organizationNumber = filter_var($organizationNumber, FILTER_SANITIZE_NUMBER_INT);
            $organizationNumber = strlen($organizationNumber) == 9
                ? $organizationNumber
                : null;
        }
        $this->data['invoiceIssuer'] = $organizationNumber;
        return $this;
    }

    /**
     * @param DateTimeInterface|null $dateOfBirth
     * @return EfakturaIdentifierKey
     */
    public function setDateOfBirth(?DateTimeInterface $dateOfBirth): EfakturaIdentifierKey
    {
        $this->data['dateOfBirth'] = $dateOfBirth ? $dateOfBirth->format('Y-m-d') : null;
        return $this;
    }

    /**
     * @param string|null $eFakturaReference
     * @return EfakturaIdentifierKey
     */
    public function setEFakturaReference(?string $eFakturaReference): EfakturaIdentifierKey
    {
        if(isset($eFakturaReference)) {
            $eFakturaReference = mb_substr(trim($eFakturaReference), 0, 32);
        }
        settype($this->data['eFakturaReferenceAndContentProvider'], 'array');
        $this->data['eFakturaReferenceAndContentProvider']['eFakturaReference'] = $eFakturaReference;
        return $this;
    }

    /**
     * @param string|null $organizationNumber
     * @return EfakturaIdentifierKey
     */
    public function setContentProviderId(?string $organizationNumber): EfakturaIdentifierKey
    {
        if ($organizationNumber) {
            $organizationNumber = filter_var($organizationNumber, FILTER_SANITIZE_NUMBER_INT);
            $organizationNumber = strlen($organizationNumber) == 9
                ? $organizationNumber
                : null;
        }
        settype($this->data['eFakturaReferenceAndContentProvider'], 'array');
        $this->data['eFakturaReferenceAndContentProvider']['contentProviderId'] = $organizationNumber;
        return $this;
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        foreach ($this->data['name'] ?? [] as $property => $value) {
            if(empty($value)) {
                unset($this->data['name'][$property]);
            }
        }
        foreach ($this->data['address'] ?? [] as $property => $value) {
            if(empty($value)) {
                unset($this->data['address'][$property]);
            }
        }
        foreach ($this->data['eFakturaReferenceAndContentProvider'] ?? [] as $property => $value) {
            if(empty($value)) {
                unset($this->data['eFakturaReferenceAndContentProvider'][$property]);
            }
        }
        if(count($this->data['eFakturaReferenceAndContentProvider'] ?? []) < 2) {
            unset($this->data['eFakturaReferenceAndContentProvider']);
        }
        foreach ($this->data as $property => $value) {
            if(empty($value)) {
                unset($this->data[$property]);
            }
        }
        return json_decode(json_encode($this->data));
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->data['id'] ?? null;
    }

    /**
     * @return bool
     */
    public function hasUniqueAlias(): bool
    {
        return (boolean)$this->getUniqueAliases();
    }

    /**
     * @return bool
     */
    public function hasPrimaryAlias(): bool
    {
        $this->getData();
        return !empty($this->data['telephoneAlias']) || !empty($this->data['emailAlias']);
    }

    /**
     * @return object|null
     */
    public function getUniqueAliases(): ?object
    {
        $this->getData();
        $keyData = null;
        if(!empty($this->data['efakturaIdentifier'])) {
            $keyData = ['efakturaIdentifier' => $this->data['efakturaIdentifier']];
        }
        if(!empty($this->data['socialSecurityNumber'])) {
            $keyData = ['socialSecurityNumber' => $this->data['socialSecurityNumber']];
        }
        if(!empty($this->data['organizationNumber'])) {
            $keyData = ['organizationNumber' => $this->data['organizationNumber']];
        }
        if(!empty($this->data['eFakturaReferenceAndContentProvider']['eFakturaReference'])
            && !empty($this->data['eFakturaReferenceAndContentProvider']['contentProviderId'])
        ) {
            $keyData = ['eFakturaReferenceAndContentProvider' => $this->data['eFakturaReferenceAndContentProvider']];
        }

        // Include invoiceIssuer
        if($keyData && !empty($this->data['invoiceIssuer'])) {
            $keyData['invoiceIssuer'] = $this->data['invoiceIssuer'];
        }
        return json_decode(json_encode($keyData));
    }

    /**
     * @return object|null
     */
    public function getPrimaryAliasesCombination(): ?object
    {
        $this->getData();
        $keyData = null;
        if(!empty($this->data['telephoneAlias'])
            && !empty($this->data['emailAlias'])
        ) {
            $keyData = [
                'telephoneAlias' => $this->data['telephoneAlias'],
                'emailAlias' => $this->data['emailAlias']
            ];
            // Include invoiceIssuer
            if(!empty($this->data['invoiceIssuer'])) {
                $keyData['invoiceIssuer'] = $this->data['invoiceIssuer'];
            }
        }
        return json_decode(json_encode($keyData));
    }

    /**
     * @return object|null
     */
    public function getPrimaryAndSecondaryAliasesCombination(): ?object
    {
        $this->getData();
        $keyData = [];
        if(!empty($this->data['telephoneAlias'])) {
            $keyData = ['telephoneAlias' => $this->data['telephoneAlias']];
        }
        if(!empty($this->data['emailAlias'])) {
            $keyData = ['emailAlias' => $this->data['emailAlias']];
        }

        if(!$keyData) {
            return null;
        }

        if(!empty($this->data['address'])) {
            $keyData['address'] = $this->data['address'];
        }
        if(!empty($this->data['dateOfBirth'])) {
            $keyData['dateOfBirth'] = $this->data['dateOfBirth'];
        }
        if(!empty($this->data['name'])) {
            $keyData['name'] = $this->data['name'];
        }

        if(count($keyData) < 2) {
            return null;
        }

        // Include invoiceIssuer
        if(!empty($this->data['invoiceIssuer'])) {
            $keyData['invoiceIssuer'] = $this->data['invoiceIssuer'];
        }
        return json_decode(json_encode($keyData));
    }

    /**
     * @return object|null
     */
    public function getSecondaryAliasesCombination(): ?object
    {
        $this->getData();
        $keyData = [];

        if(empty($this->data['address'])) {
            return null;
        }
        $aliasesCount = 1;
        if(count($this->data['address']) > 1 && !empty($this->data['address']['postalCode'])) {
            $aliasesCount = 2;
        }
        if(!empty($this->data['dateOfBirth'])) {
            $aliasesCount++;
            $keyData['dateOfBirth'] = $this->data['dateOfBirth'];
        }
        if(!empty($this->data['name'])) {
            $aliasesCount++;
            $keyData['name'] = $this->data['name'];
        }
        if($aliasesCount < 3) {
            return null;
        }
        // Include invoiceIssuer
        if(!empty($this->data['invoiceIssuer'])) {
            $keyData['invoiceIssuer'] = $this->data['invoiceIssuer'];
        }
        return json_decode(json_encode($keyData));
    }

    /**
     * @return object|null
     */
    public function getAliasCombination(): ?object
    {
        if($aliases = $this->getUniqueAliases()) {
            return $aliases;
        }
        if($aliases = $this->getPrimaryAliasesCombination()) {
            return $aliases;
        }
        if($aliases = $this->getPrimaryAndSecondaryAliasesCombination()) {
            return $aliases;
        }
        if($aliases = $this->getSecondaryAliasesCombination()) {
            return $aliases;
        }
        return null;
    }
}