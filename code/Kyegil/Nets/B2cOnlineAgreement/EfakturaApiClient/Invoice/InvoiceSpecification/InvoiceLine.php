<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\Invoice\InvoiceSpecification;

/**
 *
 */
class InvoiceLine
{
    /** @var array */
    protected array $data;

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getData());
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        foreach ($this->data as $property => $value) {
            if(empty($value)) {
                unset($this->data[$property]);
            }
        }
        return json_decode(json_encode($this->data));
    }

    /**
     * @param object|array $data
     * @return InvoiceLine
     */
    public function setData($data): InvoiceLine
    {
        $this->data = json_decode(json_encode($data), true);
        return $this;
    }

    /**
     * @param string|null $itemName
     * @return InvoiceLine
     */
    public function setItemName(?string $itemName): InvoiceLine
    {
        $itemName = isset($itemName) ? substr($itemName, 0, 200) : null;
        $this->data['itemName'] = $itemName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemName(): ?string
    {
        return $this->data['itemName'] ?? null;
    }

    /**
     * @param string|null $itemDescription
     * @return InvoiceLine
     */
    public function setItemDescription(?string $itemDescription): InvoiceLine
    {
        $itemDescription = isset($itemDescription) ? substr($itemDescription, 0, 100) : null;
        $this->data['itemDescription'] = $itemDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemDescription(): ?string
    {
        return $this->data['itemDescription'] ?? null;
    }

    /**
     * @param float|null $invoicedQuantity
     * @return InvoiceLine
     */
    public function setInvoicedQuantity(?float $invoicedQuantity): InvoiceLine
    {
        $invoicedQuantity = isset($invoicedQuantity) ? number_format($invoicedQuantity, 0, '.', '') : null;
        $this->data['invoicedQuantity'] = $invoicedQuantity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoicedQuantity(): ?string
    {
        return $this->data['invoicedQuantity'] ?? null;
    }

    /**
     * @param float|null $lineAmount
     * @return InvoiceLine
     */
    public function setLineAmount(?float $lineAmount): InvoiceLine
    {
        $lineAmount = isset($lineAmount) ? number_format($lineAmount, 0, '.', '') : null;
        $this->data['lineAmount'] = $lineAmount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLineAmount(): ?string
    {
        return $this->data['lineAmount'] ?? null;
    }

    /**
     * @param float|null $taxPercent
     * @return InvoiceLine
     */
    public function setTaxPercent(?float $taxPercent): InvoiceLine
    {
        $taxPercent = isset($taxPercent) ? number_format($taxPercent, 0, '.', '') : null;
        $this->data['taxPercent'] = $taxPercent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTaxPercent(): ?string
    {
        return $this->data['taxPercent'] ?? null;
    }

    /**
     * @param float|null $taxAmount
     * @return InvoiceLine
     */
    public function setTaxAmount(?float $taxAmount): InvoiceLine
    {
        $taxAmount = isset($taxAmount) ? number_format($taxAmount, 0, '.', '') : null;
        $this->data['taxAmount'] = $taxAmount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTaxAmount(): ?string
    {
        return $this->data['taxAmount'] ?? null;
    }

    /**
     * @param string|null $itemIdentification
     * @return InvoiceLine
     */
    public function setItemIdentification(?string $itemIdentification): InvoiceLine
    {
        $itemIdentification = isset($itemIdentification) ? substr($itemIdentification, 0, 100) : null;
        $this->data['itemIdentification'] = $itemIdentification;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemIdentification(): ?string
    {
        return $this->data['itemIdentification'] ?? null;
    }

    /**
     * @param float|null $itemPrice
     * @return InvoiceLine
     */
    public function setItemPrice(?float $itemPrice): InvoiceLine
    {
        $itemPrice = isset($itemPrice) ? number_format($itemPrice, 0, '.', '') : null;
        $this->data['itemPrice'] = $itemPrice;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemPrice(): ?string
    {
        return $this->data['itemPrice'] ?? null;
    }
}