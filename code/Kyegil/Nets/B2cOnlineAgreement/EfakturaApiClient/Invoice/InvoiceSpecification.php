<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\Invoice;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\Invoice\InvoiceSpecification\InvoiceLine;

/**
 *
 */
class InvoiceSpecification
{
    /** @var array */
    protected array $data;

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getData());
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        foreach ($this->data as $property => $value) {
            if(empty($value)) {
                unset($this->data[$property]);
            }
        }
        return json_decode(json_encode($this->data));
    }

    /**
     * @param object|array $data
     * @return InvoiceSpecification
     */
    public function setData($data): InvoiceSpecification
    {
        $this->data = json_decode(json_encode($data), true);
        return $this;
    }

    /**
     * @param string|null $firstName
     * @return InvoiceSpecification
     */
    public function setReceiverFirstName(?string $firstName): InvoiceSpecification
    {
        $firstName = isset($firstName) ? substr($firstName, 0, 100) : null;
        $this->data['receiverName']['firstName'] = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverFirstName(): ?string
    {
        return $this->data['receiverName']['firstName'] ?? null;
    }

    /**
     * @param string|null $lastName
     * @return InvoiceSpecification
     */
    public function setReceiverLastName(?string $lastName): InvoiceSpecification
    {
        $lastName = isset($lastName) ? substr($lastName, 0, 100) : null;
        $this->data['receiverName']['lastName'] = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverLastName(): ?string
    {
        return $this->data['receiverName']['lastName'] ?? null;
    }

    /**
     * @return object|null
     */
    public function getReceiverName(): ?object
    {
        return
            $this->data['receiverName']['firstName']
            || $this->data['receiverName']['lastName']
                ? (object)$this->data['receiverName']
                : null;
    }

    /**
     * @param string|null $addressLine1
     * @return InvoiceSpecification
     */
    public function setReceiverAddressLine1(?string $addressLine1): InvoiceSpecification
    {
        $addressLine1 = isset($addressLine1) ? substr($addressLine1, 0, 50) : null;
        $this->data['receiverAddress']['addressLine1'] = $addressLine1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverAddressLine1(): ?string
    {
        return $this->data['receiverAddress']['addressLine1'] ?? null;
    }

    /**
     * @param string|null $addressLine2
     * @return InvoiceSpecification
     */
    public function setReceiverAddressLine2(?string $addressLine2): InvoiceSpecification
    {
        $addressLine2 = isset($addressLine2) ? substr($addressLine2, 0, 50) : null;
        $this->data['receiverAddress']['addressLine2'] = $addressLine2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverAddressLine2(): ?string
    {
        return $this->data['receiverAddress']['addressLine2'] ?? null;
    }

    /**
     * @param string|null $postalCode
     * @return InvoiceSpecification
     */
    public function setReceiverPostalCode(?string $postalCode): InvoiceSpecification
    {
        $postalCode = isset($postalCode) ? substr($postalCode, 0, 10) : null;
        $this->data['receiverAddress']['postalCode'] = $postalCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverPostalCode(): ?string
    {
        return $this->data['receiverAddress']['postalCode'] ?? null;
    }

    /**
     * @param string|null $city
     * @return InvoiceSpecification
     */
    public function setReceiverCity(?string $city): InvoiceSpecification
    {
        $city = isset($city) ? substr($city, 0, 30) : null;
        $this->data['receiverAddress']['city'] = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverCity(): ?string
    {
        return $this->data['receiverAddress']['city'] ?? null;
    }

    /**
     * @param string|null $countryCode
     * @return InvoiceSpecification
     */
    public function setReceiverCountryCode(?string $countryCode): InvoiceSpecification
    {
        $countryCode = strlen($countryCode) == 2 ? $countryCode : null;
        $this->data['receiverAddress']['countryCode'] = $countryCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverCountryCode(): ?string
    {
        return $this->data['receiverAddress']['countryCode'] ?? null;
    }

    /**
     * @return object|null
     */
    public function getReceiverAddress(): ?object
    {
        return
            $this->data['receiverAddress']['addressLine1']
            || $this->data['receiverAddress']['addressLine2']
            || $this->data['receiverAddress']['postalCode']
            || $this->data['receiverAddress']['city']
            || $this->data['receiverAddress']['countryCode']
                ? (object)$this->data['receiverName']
                : null;
    }

    /**
     * @param string|null $firstName
     * @return InvoiceSpecification
     */
    public function setSenderFirstName(?string $firstName): InvoiceSpecification
    {
        $firstName = isset($firstName) ? substr($firstName, 0, 100) : null;
        $this->data['senderName']['firstName'] = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderFirstName(): ?string
    {
        return $this->data['senderName']['firstName'] ?? null;
    }

    /**
     * @param string|null $lastName
     * @return InvoiceSpecification
     */
    public function setSenderLastName(?string $lastName): InvoiceSpecification
    {
        $lastName = isset($lastName) ? substr($lastName, 0, 100) : null;
        $this->data['senderName']['lastName'] = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderLastName(): ?string
    {
        return $this->data['senderName']['lastName'] ?? null;
    }

    /**
     * @return object|null
     */
    public function getSenderName(): ?object
    {
        return
            $this->data['senderName']['firstName']
            || $this->data['senderName']['lastName']
                ? (object)$this->data['senderName']
                : null;
    }

    /**
     * @param string|null $addressLine1
     * @return InvoiceSpecification
     */
    public function setSenderAddressLine1(?string $addressLine1): InvoiceSpecification
    {
        $addressLine1 = isset($addressLine1) ? substr($addressLine1, 0, 50) : null;
        $this->data['senderAddress']['addressLine1'] = $addressLine1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderAddressLine1(): ?string
    {
        return $this->data['senderAddress']['addressLine1'] ?? null;
    }

    /**
     * @param string|null $addressLine2
     * @return InvoiceSpecification
     */
    public function setSenderAddressLine2(?string $addressLine2): InvoiceSpecification
    {
        $addressLine2 = isset($addressLine2) ? substr($addressLine2, 0, 50) : null;
        $this->data['senderAddress']['addressLine2'] = $addressLine2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderAddressLine2(): ?string
    {
        return $this->data['senderAddress']['addressLine2'] ?? null;
    }

    /**
     * @param string|null $postalCode
     * @return InvoiceSpecification
     */
    public function setSenderPostalCode(?string $postalCode): InvoiceSpecification
    {
        $postalCode = isset($postalCode) ? substr($postalCode, 0, 10) : null;
        $this->data['senderAddress']['postalCode'] = $postalCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderPostalCode(): ?string
    {
        return $this->data['senderAddress']['postalCode'] ?? null;
    }

    /**
     * @param string|null $city
     * @return InvoiceSpecification
     */
    public function setSenderCity(?string $city): InvoiceSpecification
    {
        $city = isset($city) ? substr($city, 0, 30) : null;
        $this->data['senderAddress']['city'] = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderCity(): ?string
    {
        return $this->data['senderAddress']['city'] ?? null;
    }

    /**
     * @param string|null $countryCode
     * @return InvoiceSpecification
     */
    public function setSenderCountryCode(?string $countryCode): InvoiceSpecification
    {
        $countryCode = strlen($countryCode) == 2 ? $countryCode : null;
        $this->data['senderAddress']['countryCode'] = $countryCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderCountryCode(): ?string
    {
        return $this->data['senderAddress']['countryCode'] ?? null;
    }

    /**
     * @return object|null
     */
    public function getSenderAddress(): ?object
    {
        return
            $this->data['senderAddress']['addressLine1']
            || $this->data['senderAddress']['addressLine2']
            || $this->data['senderAddress']['postalCode']
            || $this->data['senderAddress']['city']
            || $this->data['senderAddress']['countryCode']
                ? (object)$this->data['senderName']
                : null;
    }

    /**
     * @param DateTimeInterface|null $invoiceDate
     * @return InvoiceSpecification
     */
    public function setInvoiceDate(?DateTimeInterface $invoiceDate): InvoiceSpecification
    {
        $this->data['invoiceDate'] = null;
        if($invoiceDate) {
            if($invoiceDate instanceof DateTime) {
                $invoiceDate = DateTimeImmutable::createFromMutable($invoiceDate);
            }
            $invoiceDate = $invoiceDate->setTimezone(new DateTimeZone('UTC'));
            $this->data['invoiceDate'] = $invoiceDate->format('Y-m-d');
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceDate(): ?string
    {
        return $this->data['invoiceDate'] ?? null;
    }

    /**
     * @param string|null $paymentTerms
     * @return InvoiceSpecification
     */
    public function setPaymentTerms(?string $paymentTerms): InvoiceSpecification
    {
        $paymentTerms = isset($paymentTerms) ? substr($paymentTerms, 0, 100) : null;
        $this->data['paymentTerms'] = $paymentTerms;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaymentTerms(): ?string
    {
        return $this->data['paymentTerms'] ?? null;
    }

    /**
     * @param string $key
     * @param string|null $value
     * @return InvoiceSpecification
     */
    public function setCustomValue(string $key, ?string $value): InvoiceSpecification
    {
        $key = substr($key, 0, 20);
        $value = isset($value) ? substr($value, 0, 50) : null;
        $this->data['customValues'][$key] = $value;
        return $this;
    }

    /**
     * @param string|null $key
     * @return string|null
     */
    public function getCustomValues(?string $key = null): ?string
    {
        $key = isset($key) ? substr($key, 0, 20) : null;
        if (isset($key)) {
            return $this->data['customValues'][$key] ?? null;
        }
        return $this->data['customValues'] ?? null;
    }

    /**
     * @param array|null $invoiceTaxTotals
     * @return InvoiceSpecification
     */
    public function setInvoiceTaxTotals(?array $invoiceTaxTotals): InvoiceSpecification
    {
        $this->data['invoiceTaxTotals'] = $invoiceTaxTotals ?: null;
        return $this;
    }

    /**
     * @return array[]|null
     */
    public function getInvoiceTaxTotals(): ?object
    {
        return $this->data['invoiceTaxTotals'] ?? null;
    }

    /**
     * @param float|null $taxAmount
     * @return InvoiceSpecification
     */
    public function setTaxAmount(?float $taxAmount): InvoiceSpecification
    {
        $this->data['invoiceTaxTotals']['taxAmount']
            = isset($taxAmount)
            ? number_format($taxAmount, 2, '.', '')
            : null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTaxAmount(): ?string
    {
        return $this->data['invoiceTaxTotals']['taxAmount'] ?? null;
    }

    /**
     * @param float|null $taxPercent
     * @return InvoiceSpecification
     */
    public function setCategoryTaxPercent(?float $taxPercent): InvoiceSpecification
    {
        $this->data['invoiceTaxTotals']['taxAmountPerCategory']['taxPercent']
            = isset($taxPercent)
            ? number_format($taxPercent, 2, '.', '')
            : null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryTaxPercent(): ?string
    {
        return $this->data['invoiceTaxTotals']['taxAmountPerCategory']['taxPercent'] ?? null;
    }

    /**
     * @param float|null $taxPercent
     * @return InvoiceSpecification
     */
    public function setCategoryTaxableAmount(?float $taxPercent): InvoiceSpecification
    {
        $this->data['invoiceTaxTotals']['taxAmountPerCategory']['taxableAmount']
            = isset($taxPercent)
            ? number_format($taxPercent, 2, '.', '')
            : null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryTaxableAmount(): ?string
    {
        return $this->data['invoiceTaxTotals']['taxAmountPerCategory']['taxableAmount'] ?? null;
    }

    /**
     * @param float|null $taxPercent
     * @return InvoiceSpecification
     */
    public function setCategoryTaxAmount(?float $taxPercent): InvoiceSpecification
    {
        $this->data['invoiceTaxTotals']['taxAmountPerCategory']['taxAmount']
            = isset($taxPercent)
            ? number_format($taxPercent, 2, '.', '')
            : null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryTaxAmount(): ?string
    {
        return $this->data['invoiceTaxTotals']['taxAmountPerCategory']['taxAmount'] ?? null;
    }

    /**
     * @param string|null $invoiceDescription
     * @return InvoiceSpecification
     */
    public function setInvoiceDescription(?string $invoiceDescription): InvoiceSpecification
    {
        $invoiceDescription = isset($invoiceDescription) ? substr($invoiceDescription, 0, 100) : null;
        $this->data['invoiceDescription'] = $invoiceDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceDescription(): ?string
    {
        return $this->data['invoiceDescription'] ?? null;
    }

    /**
     * @param InvoiceLine[]|object[]|null $invoiceLines
     * @return InvoiceSpecification
     */
    public function setInvoiceLines(?array $invoiceLines): InvoiceSpecification
    {
        $this->data['invoiceLines'] = $invoiceLines ?: null;
        return $this;
    }

    /**
     * @param InvoiceLine $invoiceLine
     * @return InvoiceSpecification
     */
    public function addInvoiceLine(InvoiceLine $invoiceLine): InvoiceSpecification
    {
        $this->data['invoiceLines'][] = $invoiceLine->getData();
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceLines(): ?string
    {
        return $this->data['invoiceLines'] ?? null;
    }
}