<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\Invoice\InvoiceSpecification;

/**
 *
 */
class Invoice extends AbstractInvoice
{
    /** @var array */
    protected array $data;

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getData());
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        //** Remove binary data for separate hamndling */
        $pdf = $this->data['pdf'] ?? null;
        unset($this->data['pdf']);
        foreach ($this->data as $property => $value) {
            if(empty($value)) {
                unset($this->data[$property]);
            }
        }
        $data = json_decode(json_encode($this->data));
        if($pdf) {
            $this->data['pdf'] = $data->pdf = $pdf;
        }
        return $data;
    }

    /**
     * @param object|array $data
     * @return Invoice
     */
    public function setData($data): Invoice
    {
        $this->data = json_decode(json_encode($data), true);
        return $this;
    }

    /**
     * @param string|null $invoiceNumber
     * @return Invoice
     */
    public function setInvoiceNumber(?string $invoiceNumber): Invoice
    {
        $this->data['invoiceNumber'] = $invoiceNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceNumber(): ?string
    {
        return $this->data['invoiceNumber'] ?? null;
    }

    /**
     * @param string|null $efakturaIdentifier
     * @return Invoice
     */
    public function setEfakturaIdentifier(?string $efakturaIdentifier): Invoice
    {
        $this->data['efakturaIdentifier'] = $efakturaIdentifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEfakturaIdentifier(): ?string
    {
        return $this->data['efakturaIdentifier'] ?? null;
    }

    /**
     * @param string|null $socialSecurityNumber
     * @return Invoice
     */
    public function setSocialSecurityNumber(?string $socialSecurityNumber): Invoice
    {
        $this->data['socialSecurityNumber'] = $socialSecurityNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSocialSecurityNumber(): ?string
    {
        return $this->data['socialSecurityNumber'] ?? null;
    }

    /**
     * @param string|null $efakturaReference
     * @return Invoice
     */
    public function setEfakturaReference(?string $efakturaReference): Invoice
    {
        $this->data['efakturaReference'] = $efakturaReference;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEfakturaReference(): ?string
    {
        return $this->data['efakturaReference'] ?? null;
    }

    /**
     * @param string|null $issuer
     * @return Invoice
     */
    public function setIssuer(?string $issuer): Invoice
    {
        $this->data['issuer'] = $issuer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIssuer(): ?string
    {
        return $this->data['issuer'] ?? null;
    }

    /**
     * @param string|null $firstName
     * @return Invoice
     */
    public function setFirstName(?string $firstName): Invoice
    {
        $this->data['receiverName']['firstName'] = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->data['receiverName']['firstName'] ?? null;
    }

    /**
     * @param string|null $lastName
     * @return Invoice
     */
    public function setLastName(?string $lastName): Invoice
    {
        $this->data['receiverName']['lastName'] = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->data['receiverName']['lastName'] ?? null;
    }

    /**
     * @return object|null
     */
    public function getReceiverName(): ?object
    {
        return
            $this->data['receiverName']['firstName']
            || $this->data['receiverName']['lastName']
            ? (object)$this->data['receiverName']
            : null;
    }

    /**
     * @param string|null $addressLine1
     * @return Invoice
     */
    public function setAddressLine1(?string $addressLine1): Invoice
    {
        $this->data['receiverAddress']['addressLine1'] = $addressLine1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddressLine1(): ?string
    {
        return $this->data['receiverAddress']['addressLine1'] ?? null;
    }

    /**
     * @param string|null $addressLine2
     * @return Invoice
     */
    public function setAddressLine2(?string $addressLine2): Invoice
    {
        $this->data['receiverAddress']['addressLine2'] = $addressLine2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddressLine2(): ?string
    {
        return $this->data['receiverAddress']['addressLine2'] ?? null;
    }

    /**
     * @param string|null $postalCode
     * @return Invoice
     */
    public function setPostalCode(?string $postalCode): Invoice
    {
        $this->data['receiverAddress']['postalCode'] = $postalCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->data['receiverAddress']['postalCode'] ?? null;
    }

    /**
     * @param string|null $city
     * @return Invoice
     */
    public function setCity(?string $city): Invoice
    {
        $this->data['receiverAddress']['city'] = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->data['receiverAddress']['city'] ?? null;
    }

    /**
     * @param string|null $countryCode
     * @return Invoice
     */
    public function setCountryCode(?string $countryCode): Invoice
    {
        $this->data['receiverAddress']['countryCode'] = $countryCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->data['receiverAddress']['countryCode'] ?? null;
    }

    /**
     * @return object|null
     */
    public function getReceiverAddress(): ?object
    {
        return
            $this->data['receiverAddress']['addressLine1']
            || $this->data['receiverAddress']['addressLine2']
            || $this->data['receiverAddress']['postalCode']
            || $this->data['receiverAddress']['city']
            || $this->data['receiverAddress']['countryCode']
                ? (object)$this->data['receiverName']
                : null;
    }

    /**
     * @param string|null $receiverEmail
     * @return Invoice
     */
    public function setReceiverEmail(?string $receiverEmail): Invoice
    {
        $this->data['receiverEmail'] = $receiverEmail;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverEmail(): ?string
    {
        return $this->data['receiverEmail'] ?? null;
    }

    /**
     * @param string|null $receiverPhone
     * @return Invoice
     */
    public function setReceiverPhone(?string $receiverPhone): Invoice
    {
        $this->data['receiverPhone'] = $receiverPhone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverPhone(): ?string
    {
        return $this->data['receiverPhone'] ?? null;
    }

    /**
     * @param string|null $kid
     * @return Invoice
     */
    public function setKid(?string $kid): Invoice
    {
        $this->data['kid'] = $kid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKid(): ?string
    {
        return $this->data['kid'] ?? null;
    }

    /**
     * @param DateTimeInterface|null $dueDate
     * @return Invoice
     */
    public function setDueDate(?DateTimeInterface $dueDate): Invoice
    {
        $this->data['dueDate'] = null;
        if($dueDate) {
            if($dueDate instanceof DateTime) {
                $dueDate = DateTimeImmutable::createFromMutable($dueDate);
            }
            $dueDate = $dueDate->setTimezone(new DateTimeZone('UTC'));
            $this->data['dueDate'] = $dueDate->format('Y-m-d');
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDueDate(): ?string
    {
        return $this->data['dueDate'] ?? null;
    }

    /**
     * @param float|null $amount
     * @return Invoice
     */
    public function setAmount(?float $amount): Invoice
    {
        $this->data['amount']
            = isset($amount)
            ? number_format($amount, 2, '.', '')
            : null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->data['amount'] ?? null;
    }

    /**
     * @param float|null $amount
     * @return Invoice
     */
    public function setMinimumAmount(?float $amount): Invoice
    {
        $this->data['minimumAmount']
            = isset($amount)
            ? number_format($amount, 2, '.', '')
            : null;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMinimumAmount(): ?string
    {
        return $this->data['minimumAmount'] ?? null;
    }

    /**
     * @param string|null $accountNumber
     * @return Invoice
     */
    public function setAccountNumber(?string $accountNumber): Invoice
    {
        $this->data['accountNumber'] = $accountNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAccountNumber(): ?string
    {
        return $this->data['accountNumber'] ?? null;
    }

    /**
     * @param string|null $documentType EFAKTURA|AVTALEGIRO
     * @return Invoice
     */
    public function setDocumentType(?string $documentType): Invoice
    {
        $this->data['documentType'] = $documentType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentType(): ?string
    {
        return $this->data['documentType'] ?? null;
    }

    /**
     * @param string|null $invoiceDocumentType INVOICE|REMINDER|CREDIT_NOTE|COLLECTION_NOTICE|DUNNING|PAYMENT_REQUEST|ENFORCEMENT_WARNING
     * @return Invoice
     */
    public function setInvoiceDocumentType(?string $invoiceDocumentType): Invoice
    {
        $this->data['invoiceDocumentType'] = $invoiceDocumentType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceDocumentType(): ?string
    {
        return $this->data['invoiceDocumentType'] ?? null;
    }

    /**
     * @param string|null $pdf
     * @return Invoice
     */
    public function setPdf(?string $pdf): Invoice
    {
        $this->data['pdf'] = $pdf;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdf(): ?string
    {
        return $this->data['pdf'] ?? null;
    }

    /**
     * @param InvoiceSpecification|null $invoiceSpecification
     * @return Invoice
     */
    public function setInvoiceSpecification(?InvoiceSpecification $invoiceSpecification): Invoice
    {
        $this->data['invoiceSpecification'] = $invoiceSpecification->getData();
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceSpecification(): ?string
    {
        return $this->data['invoiceSpecification'] ?? null;
    }
}