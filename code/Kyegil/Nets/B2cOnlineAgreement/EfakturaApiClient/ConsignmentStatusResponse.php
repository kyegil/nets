<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient;

/**
 * @property-read string $messageIdentifier
 * @property-read string $status
 * @property-read string $rejectedReason
 * @property-read int $acceptedInvoiceCount
 * @property-read int $rejectedInvoiceCount
 * @property-read array{invoiceNumber: string, rejectedReason: string}[] $rejectedInvoices invoiceNumber rejectedReason
 */
class ConsignmentStatusResponse
{

    const STATUS_IN_PROGRESS = 'IN_PROGRESS';
    const STATUS_PROCESSED = 'PROCESSED';
    const STATUS_REJECTED = 'REJECTED';
    const STATUS_STOPPED = 'STOPPED';

    protected array $data = [
        'messageIdentifier' => null,
        'status' => null,
        'rejectedReason' => null,
        'acceptedInvoiceCount' => null,
        'rejectedInvoiceCount' => null,
        'rejectedInvoices' => [],
    ];

    /**
     * @param object|array|null $data
     */
    public function __construct($data = null)
    {
        if($data) {
            $this->setData($data);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->data);
    }

    /**
     * @param $name
     * @return string|null
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param object|array $data
     * @return InvoiceStatusResponse
     */
    public function setData($data): ConsignmentStatusResponse
    {
        $this->data = array_merge($this->data, json_decode(json_encode($data), true));
        return $this;
    }
}