<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient;

/**
 * @property-read string $issuer
 * @property-read string $invoiceNumber
 * @property-read string $status
 * @property-read string $rejectReason
 * @property-read string|null $distributed
 */
class InvoiceStatusResponse
{

    const STATUS_PROCESSED = 'PROCESSED';
    const STATUS_REJECTED = 'REJECTED';
    const STATUS_IN_PROGRESS = 'IN_PROGRESS';
    const STATUS_DUPLICATE = 'DUPLICATE';
    const STATUS_NOT_FOUND = 'NOT_FOUND';
    const STATUS_CANCELLED = 'CANCELLED';
    const OUTBOUND_CHANNEL_B2C = 'B2C';
    const OUTBOUND_CHANNEL_PRINT = 'PRINT';
    const OUTBOUND_CHANNEL_EMAIL = 'EMAIL';

    protected array $data = [
        'issuer' => null,
        'invoiceNumber' => null,
        'status' => null,
        'rejectReason' => null,
        'distributed' => null,
    ];

    /**
     * @param object|array|null $data
     */
    public function __construct($data = null)
    {
        if($data) {
            $this->setData($data);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->data);
    }

    /**
     * @param $name
     * @return string|null
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param object|array $data
     * @return InvoiceStatusResponse
     */
    public function setData($data): InvoiceStatusResponse
    {
        $this->data = array_merge($this->data, json_decode(json_encode($data), true));
        return $this;
    }
}