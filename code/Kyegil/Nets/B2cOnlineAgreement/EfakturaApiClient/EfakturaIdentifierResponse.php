<?php

namespace Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient;

/**
 *
 */
class EfakturaIdentifierResponse
{
    /** @var string */
    const UNIQUE_RECEIVER_FOUND = 'UNIQUE_RECEIVER_FOUND';

    /** @var string */
    const RECEIVER_NOT_FOUND = 'RECEIVER_NOT_FOUND';

    /** @var string */
    const LOOKUP_KEY_MISSING_DATA = 'LOOKUP_KEY_MISSING_DATA';

    /** @var string */
    const MULTIPLE_RECEIVERS = 'MULTIPLE_RECEIVERS';

    /** @var array */
    protected array $data = [
        'responseResult' => null,
        'hasAutoAcceptAgreements' => null,
        'efakturaIdentifier' => null,
        'efakturaIdentifierKey' => null,
        'issuerResponse' => [
            'blockedByReceiver' => null,
            'hasEfakturaAgreement' => null,
            'efakturaReferences' => null,
        ],
    ];

    /**
     * @param object|array|null $data
     */
    public function __construct($data = null)
    {
        if($data) {
            $this->setData($data);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->data);
    }

    /**
     * @param object|array $data
     * @return EfakturaIdentifierResponse
     */
    public function setData($data): EfakturaIdentifierResponse
    {
        $this->data = array_merge($this->data, json_decode(json_encode($data), true));
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResponseResult(): ?string
    {
        return $this->data['responseResult'];
    }

    /**
     * @return bool|null
     */
    public function getHasAutoAcceptAgreements(): ?bool
    {
        return isset($this->data['hasAutoAcceptAgreements']) ? (bool)$this->data['hasAutoAcceptAgreements'] : null;
    }

    /**
     * @return string|null
     */
    public function getEfakturaIdentifier(): ?string
    {
        return $this->data['efakturaIdentifier'];
    }

    /**
     * @return bool|null
     */
    public function getBlockedByReceiver(): ?bool
    {
        return isset($this->data['issuerResponse']['blockedByReceiver'])
            ? (bool)$this->data['issuerResponse']['blockedByReceiver']
            : null;
    }

    /**
     * @return bool|null
     */
    public function getHasEfakturaAgreement(): ?bool
    {
        return isset($this->data['issuerResponse']['hasEfakturaAgreement'])
            ? (bool)$this->data['issuerResponse']['hasEfakturaAgreement']
            : null;
    }

    /**
     * @return bool|null
     */
    public function getEfakturaReferences(): ?bool
    {
        return isset($this->data['issuerResponse']['efakturaReferences'])
            ? (bool)$this->data['issuerResponse']['efakturaReferences']
            : null;
    }

    /**
     * @return EfakturaIdentifierKey|null
     */
    public function getEfakturaIdentifierKey(): ?EfakturaIdentifierKey
    {
        if($this->data['efakturaIdentifierKey']) {
            $efakturaIdentifierKey = new EfakturaIdentifierKey();
            $efakturaIdentifierKey->setData($this->data['efakturaIdentifierKey']);
            $efakturaIdentifierKey->getData();
            return $efakturaIdentifierKey;
        }
        return null;
    }
}