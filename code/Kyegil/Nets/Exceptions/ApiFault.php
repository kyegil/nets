<?php

namespace Kyegil\Nets\Exceptions;

class ApiFault extends \Exception
{
    public $messageIdentifier = null;
}