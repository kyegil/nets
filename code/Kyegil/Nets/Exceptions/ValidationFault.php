<?php

namespace Kyegil\Nets\Exceptions;

class ValidationFault extends ApiFault
{

    public function __construct($response)
    {
        $this->messageIdentifier = $response->messageIdentifier;
        switch ($response->errorCode) {
            case 5001:
                $msg = 'Unable to find consignment';
                break;
            case 5002:
                $msg = 'Multiple consignments found for id';
                break;
            case 5004:
                $msg = 'Unable to find invoice';
                break;
            case 5005:
                $msg = 'Cancellation on paid invoice';
                break;
            case 5006:
                $msg = 'Invoice already cancelled';
                break;
            case 5007:
                $msg = 'Multiple invoices found';
                break;
            default:
                $msg = $response->errorCode ?? null;
        }
        parent::__construct($msg);
    }
}