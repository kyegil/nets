<?php
/**
 * * Part of kyegil/nets
 * Created by Kyegil
 * Date: 06/01/2021
 * Time: 16:48
 */

namespace Kyegil\Nets\Forsendelse\Oppdrag;


abstract class Transaksjon extends \Kyegil\Nets\AbstractModel
{
    public $transaksjonstype;
}