<?php
/**
 * * Part of kyegil/nets
 * Created by Kyegil
 * Date: 06/01/2021
 * Time: 15:00
 */

namespace Kyegil\Nets\Forsendelse\Oppdrag\AvtaleGiro;


use Kyegil\Nets\Forsendelse\AbstractOppdrag;

class BetalingskravOppdrag extends AbstractOppdrag
{
    public $tjeneste = 21;

    public $oppdragstype = 00;

    public $transaksjoner = [];

    public $sumBeløp;
}