<?php

namespace Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon;


use DateTime;
use Exception;
use Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon;
use stdClass;

/**
 * Class Efaktura
 * @package Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon
 */
class Efaktura extends Transaksjon
{
    const REQUIRED_DATA = [
        'tjeneste',
        'transaksjonstype',
        'transaksjonsnummer',
        'forfallsdato',
        'fakturautstederNavn',
        'fakturautstederPostnummer',
        'fakturautstederPoststed',
        'fakturatype',
        'summaryType',
        'mal',
        'fakturanummer',
        'fakturautstederKontonummer',
    ];

    /** @var string eFaktura = 42 */
    public $tjeneste = '42';

    /** @var string eFaktura = 03 */
    public $transaksjonstype = '03';

    /** @var int */
    public $transaksjonsnummer;

    /** @var DateTime */
    public $forfallsdato;

    /** @var float */
    public $beløp;

    /** @var string maks 25 tegn */
    public $kid;

    /** @var string maks 10 tegn */
    public $forkortetNavn;

    /** @var string maks 25 tegn */
    public $fremmedreferanse;

    /** @var string maks 30 tegn */
    public $fakturautstederNavn;

    /** @var int maks 4 tegn, 7 ved internasjonalt postnummer */
    public $fakturautstederPostnummer;

    /** @var string maks 25 tegn */
    public $fakturautstederPoststed;

    /** @var string maks 30 tegn */
    public $fakturautstederAdresse1;

    /** @var string maks 30 tegn */
    public $fakturautstederAdresse2;

    /** @var string maks 3 tegn */
    public $fakturautstederLandkode;

    /** @var string maks 20 tegn */
    public $fakturautstederTelefon;

    /** @var string maks 20 tegn */
    public $fakturautstederTelefaks;

    /** @var string maks 64 tegn */
    public $fakturautstederEmail;

    /** @var string maks 30 tegn */
    public $forbrukerNavn;

    /** @var string maks 30 tegn */
    public $forbrukerAdresse1;

    /** @var string maks 30 tegn */
    public $forbrukerAdresse2;

    /** @var int maks 4 tegn, 7 ved internasjonalt postnummer */
    public $forbrukerPostnummer;

    /** @var string maks 25 tegn */
    public $forbrukerPoststed;

    /** @var string maks 3 tegn */
    public $forbrukerLandkode;

    /** @var string maks 30 tegn */
    public $betalerNavn;

    /** @var string maks 30 tegn */
    public $betalerAdresse1;

    /** @var string maks 30 tegn */
    public $betalerAdresse2;

    /** @var int maks 4 tegn, 7 ved internasjonalt postnummer */
    public $betalerPostnummer;

    /** @var string maks 25 tegn */
    public $betalerPoststed;

    /** @var string maks 3 tegn */
    public $betalerLandkode;

    /** @var string maks 35 tegn */
    public $fakturatype;

    /** @var string maks 31 tegn */
    public $efakturareferanse;

    /** @var string maks 12 tegn */
    public $efakturaIdentifier;

    /** @var int maks 1 siffer 0 = Vanlig faktura, 1 = AvtaleGiro */
    public $summaryType = 0;

    /** @var int maks 2 siffer efaktura-mal 1 eller 2 */
    public $mal;

    /** @var int maks 1 siffer 1 = reklame, 0 = ikke reklame */
    public $reklame;

    /** @var string maks 25 tegn */
    public $fakturanummer;

    /** @var DateTime */
    public $fakturadato;

    /** @var int maks 11 siffer */
    public $fakturautstederKontonummer;

    /** @var string maks 18 tegn */
    public $fakturautstedersOrganisasjonsnummer;

    /** @var string maks 40 tegn */
    public $overskriftFakturakunde;

    /** @var string maks 40 tegn */
    public $overskriftFakturabetaler;

    /** @var string maks 96 tegn */
    public $overskriftDetaljer;

    /** @var string[] maks 96 tegn */
    public $invoiceDetails = [];

    /** @var string maks 20 tegn */
    public $ledetekstBeløp;

    /** @var string maks 20 tegn */
    public $ledetekstBetalingsfrist;

    /** @var string maks 20 tegn */
    public $ledetekstFakturadato;

    /** @var string maks 20 tegn */
    public $ledetekstFakturanummer;

    /** @var stdClass[] Up to 5 variable text fields vidth label (max 20 chars) and value (max 40 chars) */
    public $variableTextFields = [];

    /**
     * For Invoice Template 1: Max 5 lines, 80 characters per line
     * For Invoice Template 2: Max 5 lines, 96 characters per line
     *
     * @var string
     */
    public $freeTextBeforeInvoiceDetails;

    /**
     * For Invoice Template 1: Max 5 lines, 80 characters per line
     * For Invoice Template 2: Max 5 lines, 96 characters per line
     *
     * @var string
     */
    public $freeTextAfterInvoiceDetails;

    /**
     * All the data formatted accoring to Efaktura specifcations
     * @var object|null
     */
    protected $formattedData;

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        $this->prepareRecords();
        return implode("\n", $this->records);
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function prepareRecords(): Efaktura
    {
        $this->records = [];
        $this->formattedData = null;
        $this->validateData();

        $this->beløpspost1();
        $this->beløpspost2();
        $this->adressepost1Fakturautsteder();
        $this->adressepost2Fakturautsteder();
        $this->adressepost3Fakturautsteder();
        $this->adressepost4Fakturautsteder();
        $this->adressepost1Forbruker();
        $this->adressepost2Forbruker();
        $this->adressepost1Betaler();
        $this->adressepost2Betaler();
        $this->efakturaReferanser1();
        $this->efakturaReferanser2();
        $this->efakturaReferanser3();
        $this->spesifikasjonFasteFelter();
        $this->specificationsVariableFields();
        $this->specificationsFreeTextBeforeInvoiceDetails();
        $this->specificationsHeaders();
        $this->specificationsInvoiceItems();
        $this->specificationsFreeTextAfterInvoiceDetails();

        return $this;
    }

    /**
     * Validate the Data
     *
     * @return $this
     * @throws Exception
     */
    protected function validateData(): Efaktura
    {
        foreach(static::REQUIRED_DATA as $requiredProperty) {
            if(!isset($this->$requiredProperty)) {
                throw new Exception($requiredProperty . ' is required');
            }
        }

        if(!in_array($this->mal, [1,2])) {
            throw new Exception('template must be either 1 or 2');
        }
        if(!is_a($this->forfallsdato, DateTime::class)) {
            throw new Exception('forfallsdato must be a DateTime object');
        }
        if(!is_a($this->fakturadato, DateTime::class)) {
            throw new Exception('fakturadato must be a DateTime object');
        }
        if(!is_array($this->variableTextFields)) {
            throw new Exception('variableTextFields must be an array');
        }
        if(!is_array($this->invoiceDetails)) {
            throw new Exception('invoiceDetails must be an array');
        }
        foreach ($this->variableTextFields as $field) {
            if(!is_object($field)
                || !property_exists($field, 'label')
                || !property_exists($field, 'value')
            ) {
                throw new Exception('Each variable text field must be an object containing the properties label and value');
            }
        }
        return $this;
    }

    /**
     * @return object
     */
    private function getFormattedData(): object
    {
        if(!isset($this->formattedData)) {
            $invoiceColumnWidth = $this->mal == 2 ? 96 : 80;

            $this->formattedData = (object)[
                'transaksjonsnummer'        => $this->numFix($this->transaksjonsnummer, 7),
                'forfallsdato'              => $this->forfallsdato->format('dmy'),
                'beløp'                     => $this->numFix(round($this->beløp * 100), 17),
                'fakturadatoFormatert'      => $this->fakturadato->format('d.m.Y'),
                'forfallsdatoFormatert'     => $this->forfallsdato->format('d.m.Y'),
                'beløpFormatert'            => $this->strFix(number_format($this->beløp, 2, ',', '.'), 20),
                'fakturautstederNavn'       => $this->strFix($this->fakturautstederNavn, 30),
                'fakturautstederAdresse1'   => $this->strFix($this->fakturautstederAdresse1, 30),
                'fakturautstederAdresse2'   => $this->strFix($this->fakturautstederAdresse2, 30),
                'fakturautstederEmail'      => $this->strFix($this->fakturautstederEmail, 64),
                'fakturautstedersOrganisasjonsnummer' => $this->strFix($this->fakturautstedersOrganisasjonsnummer, 18),
                'fakturautstederPostnummer' => $this->strFix($this->fakturautstederPostnummer, 7),
                'fakturautstederPoststed'   => $this->strFix($this->fakturautstederPoststed, 25),
                'fakturautstederLandkode'   => $this->strFix($this->fakturautstederLandkode, 3),
                'fakturautstederTelefon'    => $this->strFix($this->fakturautstederTelefon, 20),
                'fakturautstederTelefaks'   => $this->strFix($this->fakturautstederTelefaks, 20),
                'kid'	                    => $this->numFix($this->kid, 25, ' '),
                'forkortetNavn'             => $this->strFix($this->forkortetNavn, 10),
                'fremmedreferanse'          => $this->strFix($this->fremmedreferanse, 25),
                'forbrukerNavn'             => $this->strFix($this->forbrukerNavn, 30),
                'forbrukerAdresse1'         => $this->strFix($this->forbrukerAdresse1, 30),
                'forbrukerAdresse2'         => $this->strFix($this->forbrukerAdresse2, 30),
                'forbrukerPostnummer'       => $this->strFix($this->forbrukerPostnummer, 7),
                'forbrukerPoststed'         => $this->strFix($this->forbrukerPoststed, 25),
                'forbrukerLandkode'         => $this->strFix($this->forbrukerLandkode, 3),
                'betalerNavn'               => $this->strFix($this->betalerNavn, 30),
                'betalerAdresse1'           => $this->strFix($this->betalerAdresse1, 30),
                'betalerAdresse2'           => $this->strFix($this->betalerAdresse2, 30),
                'betalerPostnummer'         => $this->strFix($this->betalerPostnummer, 7),
                'betalerPoststed'           => $this->strFix($this->betalerPoststed, 25),
                'betalerLandkode'           => $this->strFix($this->betalerLandkode, 3),
                'fakturatype'               => $this->strFix($this->fakturatype, 35),
                'efakturareferanse'         => $this->strFix((isset($this->efakturaIdentifier) ? ('efakturaIdentifier:' . $this->efakturaIdentifier) : $this->efakturareferanse), 31),
                'summaryType'               => $this->numFix((int)$this->summaryType, 1),
                'mal'                       => $this->numFix($this->mal, 2),
                'reklame'                   => $this->numFix((int)$this->reklame, 1),
                'fakturanummer'             => $this->strFix($this->fakturanummer, 25),
                'fakturautstederKontonummer' => $this->numFix($this->fakturautstederKontonummer, 11),

                'overskriftFakturakunde'    => $this->strFix($this->overskriftFakturakunde, 40),
                'overskriftFakturabetaler'  => $this->strFix($this->overskriftFakturabetaler, 40),
                'overskriftDetaljer'        => $this->strFix($this->overskriftDetaljer, 96),

                'ledetekstBeløp'            => $this->strFix($this->ledetekstBeløp, 20),
                'ledetekstBetalingsfrist'   => $this->strFix($this->ledetekstBetalingsfrist, 20),
                'ledetekstFakturadato'      => $this->strFix($this->ledetekstFakturadato, 20),
                'ledetekstFakturanummer'    => $this->strFix($this->ledetekstFakturanummer, 20),
            ];

            $this->formattedData->invoiceDetails = [];
            foreach($this->invoiceDetails as $detail) {
                $this->formattedData->invoiceDetails[] = $this->strFix($detail, 96);
            }

            $variableTextFields = [];
            foreach($this->variableTextFields as $field) {
                $variableTextFields[] = (object)[
                    'label' => $this->strFix($field->label, 20),
                    'value' => $this->strFix($field->value, 40)
                ];
            }
            $this->formattedData->variableTextFields = array_slice($variableTextFields, 0, 5);

            /** @var array $freeTextBeforeInvoiceDetails */
            $freeTextBeforeInvoiceDetails = preg_split('/\n|\r\n?/', wordwrap($this->freeTextBeforeInvoiceDetails, $invoiceColumnWidth, "\n", true));
            $this->formattedData->freeTextBeforeInvoiceDetails = $freeTextBeforeInvoiceDetails ? array_slice($freeTextBeforeInvoiceDetails, 0,5) : [];

            /** @var array $freeTextAfterInvoiceDetails */
            $freeTextAfterInvoiceDetails = preg_split('/\n|\r\n?/', wordwrap($this->freeTextAfterInvoiceDetails, $invoiceColumnWidth, "\n", true));
            $this->formattedData->freeTextAfterInvoiceDetails = $freeTextAfterInvoiceDetails ? array_slice($freeTextAfterInvoiceDetails, 0,5) : [];

        }
        return $this->formattedData;
    }

    /**
     * Beløpspost 1 (Record 30)
     *
     * Påkrevd for at transaksjonen skal være gyldig.
     *
     * @return $this
     */
    private function beløpspost1(): Efaktura
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '42' /* (tjenestekode		= 42 eFaktura) */
            . '03' /* (transaksjonstype	= 03 eFaktura) */
            . '30'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . $formattedData->forfallsdato
            . str_repeat(' ', 11)  /* (filler) */
            . $formattedData->beløp
            . $formattedData->kid
            . str_repeat('0', 6)  /* (filler) */
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * Beløpspost 2 (Record 31)
     *
     * Denne record er tatt med for å vise mulig fremtidig bruk.
     * Legger man til denne recorden,
     * vil nets kunne trekke et komplett avtalegiro-oppdrag ut fra efakturaen,
     * slik at man slipper å sende inn avtalegiro-oppdraget som en egen forsendelse.
     *
     * @return $this
     */
    private function beløpspost2(): Efaktura
    {
        if($this->forkortetNavn || $this->fremmedreferanse) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '31'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . $formattedData->forkortetNavn
                . str_repeat(' ', 25)  /* (filler) */
                . $formattedData->fremmedreferanse
                . str_repeat('0', 5)  /* (filler) */
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * Adressepost 1 Fakturautsteder
     *
     * Adressepost 1 Fakturautsteder (navn/postnr/sted) (rec.type 40, melding = 1)
     * Påkrevd record.
     *
     * @return $this
     */
    private function adressepost1Fakturautsteder(): Efaktura
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '42' /* (tjenestekode		= 42 eFaktura) */
            . '03' /* (transaksjonstype	= 03 eFaktura) */
            . '40'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Melding: 1 = Fakturautsteder, 2 = Forbruker, 3 = Annen betaler) */
            . $formattedData->fakturautstederNavn
            . $formattedData->fakturautstederPostnummer
            . $formattedData->fakturautstederPoststed
            . str_repeat('0', 2)  /* (filler) */
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * Adressepost 2 Fakturautsteder
     *
     * Adressepost 2 Fakturautsteder (postboks/gate/vei) (rec.type 41, melding = 1)
     * Påkrevd record.
     *
     * @return $this
     */
    private function adressepost2Fakturautsteder(): Efaktura
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '42' /* (tjenestekode		= 42 eFaktura) */
            . '03' /* (transaksjonstype	= 03 eFaktura) */
            . '41'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Melding: 1 = Fakturautsteder, 2 = Forbruker, 3 = Annen betaler) */
            . $formattedData->fakturautstederAdresse1
            . $formattedData->fakturautstederAdresse2
            . $formattedData->fakturautstederLandkode
            . str_repeat('0', 1)  /* (filler) */
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * Adressepost 3 Fakturautsteder
     *
     * Adressepost 3 Fakturautsteder (telefon/telefaks) (rec.type 22)
     * Denne record er valgfri å̊ benytte.
     * Informasjonen kan sendes inn for hver faktura eller ligge fast i Nets
     *
     * @return $this
     */
    private function adressepost3Fakturautsteder(): Efaktura
    {
        if($this->fakturautstederTelefon || $this->fakturautstederTelefaks) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '22'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '1' /* (melding	= 1 fakturautsteder) */
                . $formattedData->fakturautstederTelefon
                . $formattedData->fakturautstederTelefaks
                . str_repeat('0', 24)  /* (filler) */
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * Adressepost 4 Fakturautsteder
     *
     * Adressepost 4 Fakturautsteder (eMail) (rec.type 23)
     * Denne record er valgfri å̊ benytte.
     * Informasjonen kan sendes inn for hver faktura eller ligge fast i Nets
     *
     * @return $this
     */
    private function adressepost4Fakturautsteder(): Efaktura
    {
        if($this->fakturautstederEmail) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '23'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '1' /* (melding	= 1 fakturautsteder) */
                . $formattedData->fakturautstederEmail
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * Adressepost 1 Forbruker
     *
     * Adressepost 1 Forbruker (navn/postnr/sted) (rec.type 40, melding = 2)
     * Påkrevd record.
     *
     * @return $this
     */
    private function adressepost1Forbruker(): Efaktura
    {
        if($this->forbrukerNavn) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '40'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '2'  /* (Melding: 1 = Fakturautsteder, 2 = Forbruker, 3 = Annen betaler) */
                . $formattedData->forbrukerNavn
                . $formattedData->forbrukerPostnummer
                . $formattedData->forbrukerPoststed
                . str_repeat('0', 2)  /* (filler) */
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * Adressepost 2 Forbruker
     *
     * Adressepost 2 Forbruker (postboks/gate/vei) (rec.type 41, melding = 2)
     * Påkrevd record.
     *
     * @return $this
     */
    private function adressepost2Forbruker(): Efaktura
    {
        if($this->forbrukerNavn) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '41'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '2'  /* (Melding: 1 = Fakturautsteder, 2 = Forbruker, 3 = Annen betaler) */
                . $formattedData->forbrukerAdresse1
                . $formattedData->forbrukerAdresse2
                . $formattedData->forbrukerLandkode
                . str_repeat('0', 1)  /* (filler) */
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * Adressepost 1 Betaler
     *
     * Adressepost 1 Betaler (navn/postnr/sted) (rec.type 40, melding = 2)
     * Påkrevd record.
     *
     * @return $this
     */
    private function adressepost1Betaler(): Efaktura
    {
        if($this->betalerNavn) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '40'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '3'  /* (Melding: 1 = Fakturautsteder, 2 = Forbruker, 3 = Annen betaler) */
                . $formattedData->betalerNavn
                . $formattedData->betalerPostnummer
                . $formattedData->betalerPoststed
                . str_repeat('0', 2)  /* (filler) */
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * Adressepost 2 Betaler
     *
     * Adressepost 2 Betaler (postboks/gate/vei) (rec.type 41, melding = 2)
     * Påkrevd record.
     *
     * @return $this
     */
    private function adressepost2Betaler(): Efaktura
    {
        if($this->betalerNavn) {
            $formattedData = $this->getFormattedData();
            $record = 'NY' /* (formatkode) */
                . '42' /* (tjenestekode		= 42 eFaktura) */
                . '03' /* (transaksjonstype	= 03 eFaktura) */
                . '41'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '3'  /* (Melding: 1 = Fakturautsteder, 2 = Forbruker, 3 = Annen betaler) */
                . $formattedData->betalerAdresse1
                . $formattedData->betalerAdresse2
                . $formattedData->betalerLandkode
                . str_repeat('0', 1)  /* (filler) */
            ;
            $this->records[] = $record;
        }
        return $this;
    }

    /**
     * eFaktura-referanser 1
     *
     * eFaktura-referanser 1 (rec.type 34)
     * Påkrevd record.
     *
     * @return $this
     */
    private function efakturaReferanser1(): Efaktura
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '42' /* (tjenestekode		= 42 eFaktura) */
            . '03' /* (transaksjonstype	= 03 eFaktura) */
            . '34'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . $formattedData->forfallsdatoFormatert
            . $formattedData->beløpFormatert
            . $formattedData->fakturatype
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * eFaktura-referanser 2
     *
     * eFaktura-referanser 2 (rec.type 35)
     * Påkrevd record.
     *
     * @return $this
     */
    private function efakturaReferanser2(): Efaktura
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '42' /* (tjenestekode		= 42 eFaktura) */
            . '03' /* (transaksjonstype	= 03 eFaktura) */
            . '35'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . $formattedData->efakturareferanse
            . $formattedData->summaryType
            . $formattedData->mal
            . $formattedData->reklame
            . $formattedData->fakturautstederNavn
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * eFaktura-referanser 3
     *
     * eFaktura-referanser 3 (rec.type 36)
     * Påkrevd record.
     *
     * @return $this
     */
    private function efakturaReferanser3(): Efaktura
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '42' /* (tjenestekode		= 42 eFaktura) */
            . '03' /* (transaksjonstype	= 03 eFaktura) */
            . '36'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . $formattedData->fakturanummer
            . $formattedData->fakturadatoFormatert
            . $formattedData->fakturautstederKontonummer
            . $formattedData->fakturautstedersOrganisasjonsnummer
            . str_repeat('0', 1)  /* (filler) */
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * Spesifikasjonsrecords for faste felter
     *
     * Spesifisering av faste felter på faktura (12 records)
     * Dette er ledeteksten til de påkrevde feltene i fakturahodet og må således alltid spesifiseres.
     * Hvis ledeteksten ikke spesifiseres, vil tekstene vist i Fakturamal 1 komme som standard.
     *
     * De 4 feltene er:
     * 001 - Beløp til forfall
     * 002 – Betalingsfrist
     * 003 – Fakturadato
     * 004 – Fakturanummer
     *
     * Kolonne 2 og 3 i hvert felt er blanke.
     *
     * @return $this
     */
    private function spesifikasjonFasteFelter(): Efaktura
    {
        $formattedData = $this->getFormattedData();

        /**
         * Ledetekst Beløp
         */
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '001'  /* (Plassering / Linje 001: ledetekst foran Beløp til forfall */
            . '1'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . $formattedData->ledetekstBeløp
            . str_repeat(' ', 20)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '001'  /* (Plassering / Linje 001: ledetekst foran Beløp til forfall */
            . '2'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '001'  /* (Plassering / Linje 001: ledetekst foran Beløp til forfall */
            . '3'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;

        /**
         * Ledetekst Betalingsfrist
         */
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '002'  /* (Plassering / Linje 002: ledetekst foran Betalingsfrist */
            . '1'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . $formattedData->ledetekstBetalingsfrist
            . str_repeat(' ', 20)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '002'  /* (Plassering / Linje 002: ledetekst foran Betalingsfrist */
            . '2'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '002'  /* (Plassering / Linje 002: ledetekst foran Betalingsfrist */
            . '3'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;

        /**
         * Ledetekst Fakturadato
         */
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '003'  /* (Plassering / Linje 003: ledetekst foran Fakturadato */
            . '1'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . $formattedData->ledetekstFakturadato
            . str_repeat(' ', 20)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '003'  /* (Plassering / Linje 003: ledetekst foran Fakturadato */
            . '2'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '003'  /* (Plassering / Linje 003: ledetekst foran Fakturadato */
            . '3'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;

        /**
         * Ledetekst Fakturanummer
         */
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '004'  /* (Plassering / Linje 004: ledetekst foran Fakturanummer */
            . '1'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . $formattedData->ledetekstFakturanummer
            . str_repeat(' ', 20)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '004'  /* (Plassering / Linje 004: ledetekst foran Fakturanummer */
            . '2'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
            . $formattedData->transaksjonsnummer
            . '1'  /* (Spesifikasjon 1 = Spesifisering av faste felter) */
            . '004'  /* (Plassering / Linje 004: ledetekst foran Fakturanummer */
            . '3'  /* (Kolonne: For faste felter brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke) */
            . str_repeat(' ', 40)  /* (filler) */
            . str_repeat('0', 20)  /* (filler) */
        ;
        return $this;
    }

    /**
     * @return $this
     */
    private function specificationsVariableFields(): Efaktura {
        $formattedData = $this->getFormattedData();
        /**
         * @var int $line
         * @var stdClass $field
         */
        foreach ($formattedData->variableTextFields as $line => $field) {
            $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '2'  /* (Spesifikasjon 2 = Spesifisering av variable felter) */
                . '00' . ($line + 1)  /* (Line #) */
                . '1'  /* (Kolonne: For variable felter brukes kolonne 1 som ledetekst og kolonne 2 for feltverdi, mens kolonne 3 må sendes blank) */
                . $field->label
                . str_repeat(' ', 20)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '2'  /* (Spesifikasjon 2 = Spesifisering av variable felter) */
                . '00' . ($line + 1)  /* (Line #) */
                . '2'  /* (Kolonne: For variable felter brukes kolonne 1 som ledetekst og kolonne 2 for feltverdi, mens kolonne 3 må sendes blank) */
                . $field->value
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] = 'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '2'  /* (Spesifikasjon 2 = Spesifisering av variable felter) */
                . '00' . ($line + 1)  /* (Line #) */
                . '3'  /* (Kolonne: For variable felter brukes kolonne 1 som ledetekst og kolonne 2 for feltverdi, mens kolonne 3 må sendes blank) */
                . str_repeat(' ', 40)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }
        return $this;
    }

    /**
     * Free Text Before Invoice Details
     *
     * 3 records per text line.
     * There can be up to 5 text lines.
     * Each line can have 96 characters when using template 2,
     * or 80 characters when using template 1
     *
     * @return $this
     */
    private function specificationsFreeTextBeforeInvoiceDetails(): Efaktura {
        $formattedData = $this->getFormattedData();
        /**
         * @var int $lineNo
         * @var string $lineContent
         */
        foreach ($formattedData->freeTextBeforeInvoiceDetails as $lineNo => $lineContent) {
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
                . $formattedData->transaksjonsnummer
                . '3'  /* (Spesifikasjon 3 = Spesifisering av fritekst før fakturadetaljer) */
                . '00' . ( $lineNo + 1 )  /* (Line Number) */
                . '1'  /* (Kolonne) */
                . $this->strFix( mb_substr($lineContent, 0, 40, 'UTF-8'), 40)
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
                . $formattedData->transaksjonsnummer
                . '3'  /* (Spesifikasjon 3 = Spesifisering av fritekst før fakturadetaljer) */
                . '00' . ( $lineNo + 1 )  /* (Line Number) */
                . '2'  /* (Kolonne) */
                . $this->strFix( mb_substr($lineContent, 40, 40, 'UTF-8'), 40)
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
                . $formattedData->transaksjonsnummer
                . '3'  /* (Spesifikasjon 3 = Spesifisering av fritekst før fakturadetaljer) */
                . '00' . ( $lineNo + 1 )  /* (Line Number) */
                . '3'  /* (Kolonne) */
                . $this->strFix( mb_substr($lineContent, 80, 16, 'UTF-8'), 16)
                . str_repeat(' ', 24)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }

        return $this;
    }

    /**
     * Specifications for Headers (Record Type 49 Specification Records)
     *
     * Specifications records for Headers
     * 3 records per header field
     *
     * Headers placements:
     * 001 – Customer Header
     * 002 –Payer Header
     *
     * @return $this
     */
    private function specificationsHeaders(): Efaktura {
        $formattedData = $this->getFormattedData();

        /**
         * Header for Customer
         */
        if($this->overskriftFakturakunde) {
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '001'  /* (Plassering) 001: overskrift over Fakturakunde */
                . '1'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . $formattedData->overskriftFakturakunde
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '001'  /* (Plassering) 001: overskrift over Fakturakunde */
                . '2'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . str_repeat(' ', 40)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '001'  /* (Plassering) 001: overskrift over Fakturakunde */
                . '3'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . str_repeat(' ', 40)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }

        /**
         * Header for Payer
         */
        if($this->overskriftFakturabetaler) {
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '002'  /* (Plassering) 002: overskrift over Fakturabetaler */
                . '1'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . $formattedData->overskriftFakturabetaler
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '002'  /* (Plassering) 001: overskrift over Fakturabetaler */
                . '2'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . str_repeat(' ', 40)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '002'  /* (Plassering) 001: overskrift over Fakturabetaler */
                . '3'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . str_repeat(' ', 40)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }

        /**
         * Header for Invoice Specification lines
         */
        if($this->overskriftDetaljer) {
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '003'  /* (Plassering) 003: overskrift over Fakturaspesifikasjon */
                . '1'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . mb_substr($formattedData->overskriftDetaljer, 0, 40)
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '003'  /* (Plassering) 003: overskrift over Fakturaspesifikasjon */
                . '2'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . mb_substr($formattedData->overskriftDetaljer, 40, 40)
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (Spesifikasjon 4 = Spesifisering av overskrifter) */
                . '003'  /* (Plassering) 003: overskrift over Fakturaspesifikasjon */
                . '3'  /* (Plassering / Kolonne) For overskrift over fakturakunde brukes kolonne 1, mens kolonne 2 og 3 må sendes blanke */
                . mb_substr($formattedData->overskriftDetaljer, 80, 16)
                . str_repeat(' ', 24)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }
        return $this;
    }

    /**
     * Specifications for Invoice Details (Record Type 49 Specification Records)
     *
     * Specifications for Invoice Details
     * 3 records per Item Line
     * Dersom blank linje ønskes, må det sendes linjer med spaces i hver kolonne. Alle tre kolonner må fylles ut hver gang.
     * Dersom kolonne 3 ikke benyttes, fordi fakturautsteder har valgt å bruke Mal 1, skal kolonne 3 fylles ut med blanke (spaces)
     *
     * @return $this
     */
    private function specificationsInvoiceItems(): Efaktura {
        $formattedData = $this->getFormattedData();
        foreach($this->invoiceDetails as $lineNo => $lineItem) {
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '5'  /* (Spesifikasjon 5 = fakturadetaljer) */
                . $this->numFix( $lineNo + 1, 3)  /* (Line #) */
                . '1'  /* (Kolonne) */
                . mb_substr( $lineItem, 0, 40, 'UTF-8')
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '5'  /* (Spesifikasjon 5 = fakturadetaljer) */
                . $this->numFix( $lineNo + 1, 3)  /* (Line #) */
                . '2'  /* (Kolonne) */
                . mb_substr( $lineItem, 40, 40, 'UTF-8')
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype 49) */
                . $formattedData->transaksjonsnummer
                . '5'  /* (Spesifikasjon 5 = fakturadetaljer) */
                . $this->numFix( $lineNo + 1, 3)  /* (Line #) */
                . '3'  /* (Kolonne) */
                . mb_substr( $lineItem, 80, 16, 'UTF-8')
                . str_repeat(' ', 24)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }
        return $this;
    }

    /**
     * Free Text After Invoice Details
     *
     * 3 records per text line.
     * There can be up to 5 text lines.
     * Each line can have 96 characters when using template 2,
     * or 80 characters when using template 1
     *
     * @return $this
     */
    private function specificationsFreeTextAfterInvoiceDetails(): Efaktura {
        $formattedData = $this->getFormattedData();
        /**
         * @var int $lineNo
         * @var string $lineContent
         */
        foreach ($formattedData->freeTextAfterInvoiceDetails as $lineNo => $lineContent) {
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
                . $formattedData->transaksjonsnummer
                . '6'  /* (Spesifikasjon 6 = Spesifisering av fritekst etter fakturadetaljer) */
                . '00' . ( $lineNo + 1 )  /* (Line Number) */
                . '1'  /* (Kolonne) */
                . $this->strFix( mb_substr($lineContent, 0, 40, 'UTF-8'), 40)
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
                . $formattedData->transaksjonsnummer
                . '6'  /* (Spesifikasjon 6 = Spesifisering av fritekst etter fakturadetaljer) */
                . '00' . ( $lineNo + 1 )  /* (Line Number) */
                . '2'  /* (Kolonne) */
                . $this->strFix( mb_substr($lineContent, 40, 40, 'UTF-8'), 40)
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] =
                'NY420349' /* (formatkode NY, tjenestekode 42, transaksjonstype 03, recordtype49) */
                . $formattedData->transaksjonsnummer
                . '6'  /* (Spesifikasjon 6 = Spesifisering av fritekst etter fakturadetaljer) */
                . '00' . ( $lineNo + 1 )  /* (Line Number) */
                . '3'  /* (Kolonne) */
                . $this->strFix( mb_substr($lineContent, 80, 16, 'UTF-8'), 16)
                . str_repeat(' ', 24)  /* (filler) */
                . str_repeat('0', 20)  /* (filler) */
            ;
        }

        return $this;
    }
}