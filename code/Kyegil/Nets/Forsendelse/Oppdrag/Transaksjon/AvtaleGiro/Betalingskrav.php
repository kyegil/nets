<?php

namespace Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon\AvtaleGiro;


use DateTime;
use Exception;
use Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon;

class Betalingskrav extends Transaksjon
{
    const REQUIRED_DATA = [
        'tjeneste',
        'oppdragstype',
        'transaksjonstype',
        'transaksjonsnummer',
        'forfallsdato',
        'beløp',
        'kid',
    ];

    const TYPE_VARSLING_FRA_BANK = '21';
    const TYPE_EGENVARSEL = '02';
    const TRANSAKSJONSTYPER = [
        '02' => 'Ingen varsling fra bank',
        '21' => 'Varsling fra bank (AvtaleGiro info)'
    ];

    /** @var string AvtaleGiro = 21 */
    public $tjeneste = '21';

    /** @var string 00 for nye betalingskrav */
    public $oppdragstype = '00';

    /** @var string '02'|'21' */
    public $transaksjonstype = '21';

    /**
     * Transaksjonsnummer
     *
     * Unik nummerering av transaksjonen pr. oppdrag i stigende sekvens
     * Transaksjonsnummeret må være større enn null.
     *
     * @var int
     */
    public $transaksjonsnummer;

    /**
     * Må ikke være mer enn 12 måneder frem i tid
     *
     * Ved bruk av kode 02 ”ingen varsel fra bank” og betalingsmottaker har avtalt sms varsling
     * kan max forfall settes til 50 kalenderdager frem i tid.
     * Transaksjoner med forfall mer enn 50 kalenderdager frem i tid vil bli avvist ved mottak
     *
     * @var \DateTime
     */
    public $forfallsdato;

    /**
     * Mobilnummer
     *
     * Har betalingsmottaker avtale om å varsle betaler via sms oppgis betalers mobilnr
     * Max 8 siffer
     *
     * @var string
     */
    public $mobilnr;

    /** @var float */
    public $beløp;

    /** @var string */

    /**
     * Feltet må fylles ut hvis betalingskravet skal være gyldig mot betalers faste oppdrag.
     * Fylles ut med gyldig KID etter spesifikasjoner angitt i registreringsskjema for AvtaleGiro og OCR giro
     * Bokstaver kan ikke benyttes
     *
     * @var string
     */
    public $kid;

    /**
     * Fylles ut med forkortet navn for betaler
     *
     * Feltet bør fylles ut for å lette identifikasjon av evt. avviste betalingskrav
     *
     * @var string
     */
    public $forkortetNavn;

    /**
     * Alfanumerisk, 25 posisjoner
     *
     * Feltet kan benyttes som et opplysningsfelt om betalingskravet overfor betaler.
     * Fremmedreferansen overføres til betalers kontoutskrift og AvtaleGiro info.
     * Dersom fremmedreferansen ikke benyttes skal feltet blankes.
     * Fremmedreferansen overstyrer fast tekst på betalers faste betalingsoppdrag.
     * Ved bruk av sms vil teksten i fremmedreferansen også bli oversendt i sms til betaler
     *
     * @var string
     */
    public $fremmedreferanse;

    /**
     * Spesifikasjon over flere linjer
     *
     * Maks 42 linjer á 80 tegn
     *
     * @var string
     */
    public $spesifikasjon;

    /**
     * All the data formatted accoring to AvtaleGiro specifcations
     *
     * @var object|null
     */
    protected $formattedData;

    /**
     * @return int|string
     */
    public function __toString()
    {
        $this->prepareRecords();
        return implode("\n", $this->records);
    }

    /**
     * @return $this
     */
    public function prepareRecords(): Betalingskrav {
        $this->records = [];
        $this->formattedData = null;
        $this->validateData();

        $this->beløpspost1();
        $this->beløpspost2();
        $this->spesifikasjonsrecords();

        return $this;
    }

    /**
     * @return $this
     */
    protected function validateData(): Betalingskrav {
        foreach(static::REQUIRED_DATA as $requiredProperty) {
            if(!isset($this->$requiredProperty)) {
                throw new Exception($requiredProperty . ' is required');
            }
        }

        if(!in_array($this->transaksjonstype, array_keys(self::TRANSAKSJONSTYPER))) {
            throw new Exception('transaksjonstype must be either 02 or 21');
        }
        if(!is_a($this->forfallsdato, DateTime::class)) {
            throw new Exception('forfallsdato must be a DateTime object');
        }
        return $this;
    }

    private function getFormattedData(): object
    {
        if(!isset($this->formattedData)) {
            $this->formattedData = (object)[
                'transaksjonsnummer'        => $this->numFix($this->transaksjonsnummer, 7),
                'forfallsdato'              => $this->forfallsdato->format('dmy'),
                'mobilnr'                   => $this->numFix($this->mobilnr, 11, ' '),
                'beløp'                     => $this->numFix(round($this->beløp * 100), 17),
                'kid'	                    => $this->numFix($this->kid, 25, ' '),
                'forkortetNavn'	            => $this->strFix($this->forkortetNavn, 10),
                'fremmedreferanse'          => $this->strFix($this->fremmedreferanse, 25),
            ];

            $spesifikasjon = wordwrap( $this->spesifikasjon, 80, "\n", true );
            $spesifikasjonslinjer = trim($spesifikasjon) ? explode("\n", $spesifikasjon) : [];
            $this->formattedData->spesifikasjon = array_slice($spesifikasjonslinjer, 0,42);
        }
        return $this->formattedData;
    }

    /**
     * Beløpspost 1 (Record 30)
     *
     * Påkrevd for at transaksjonen skal være gyldig.
     *
     * @return $this
     */
    private function beløpspost1(): Betalingskrav {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '21' /* (tjenestekode		= 21 AvtaleGiro) */
            . $this->transaksjonstype /* (transaksjonstype	= 02 eller 21) */
            . '30'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . $formattedData->forfallsdato
            . $formattedData->mobilnr
            . $formattedData->beløp
            . $formattedData->kid
            . str_repeat('0', 6)  /* (filler) */
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * Beløpspost 2 (Record 31)
     *
     * Påkrevd for at transaksjonen skal være gyldig.
     *
     * @return $this
     */
    private function beløpspost2(): Betalingskrav
    {
        $formattedData = $this->getFormattedData();
        $record = 'NY' /* (formatkode) */
            . '21' /* (tjenestekode		= 21 AvtaleGiro) */
            . $this->transaksjonstype /* (transaksjonstype	= 02 eller 21) */
            . '31'  /* (recordtype) */
            . $formattedData->transaksjonsnummer
            . $formattedData->forkortetNavn
            . str_repeat(' ', 25)  /* (filler) */
            . $formattedData->fremmedreferanse
            . str_repeat('0', 5)  /* (filler) */
        ;
        $this->records[] = $record;
        return $this;
    }

    /**
     * Beløpspost 2 (Record 31)
     *
     * Påkrevd for at transaksjonen skal være gyldig.
     *
     * @return $this
     */
    private function spesifikasjonsrecords(): Betalingskrav
    {
        $formattedData = $this->getFormattedData();
        $linje = 1;
        foreach ($this->formattedData->spesifikasjon as $tekstlinje) {
            $kolonne1 = mb_substr($tekstlinje, 0, 40);
            $kolonne2 = mb_substr($tekstlinje, 40);

            $record = 'NY' /* (formatkode) */
                . '21' /* (tjenestekode		= 21 AvtaleGiro) */
                . $this->transaksjonstype /* (transaksjonstype	= 02 eller 21) */
                . '49'  /* (recordtype) */
                . $formattedData->transaksjonsnummer
                . '4'  /* (betalingsvarsel) */
                . $this->numFix($linje, 3) /* (Plassering/linje) */
                . '1'  /* (Plassering/kolonne) */
                . $this->strFix($kolonne1, 40) /* (Meldingsspesifikasjon tekstdel) */
                . str_repeat('0', 20)  /* (filler) */
            ;
            $this->records[] = $record;

            if (trim($kolonne2)) {
                $record = 'NY' /* (formatkode) */
                    . '21' /* (tjenestekode		= 21 AvtaleGiro) */
                    . $this->transaksjonstype /* (transaksjonstype	= 02 eller 21) */
                    . '49'  /* (recordtype) */
                    . $formattedData->transaksjonsnummer
                    . '4'  /* (betalingsvarsel) */
                    . $this->numFix($linje, 3) /* (Plassering/linje) */
                    . '2'  /* (Plassering/kolonne) */
                    . $this->strFix($kolonne2, 40) /* (Meldingsspesifikasjon tekstdel) */
                    . str_repeat('0', 20)  /* (filler) */
                ;
                $this->records[] = $record;
            }

            $linje++;
        }
        return $this;
    }
}