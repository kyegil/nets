<?php
/**
 * * Part of kyegil/nets
 * Created by Kyegil
 * Date: 06/01/2021
 * Time: 15:00
 */

namespace Kyegil\Nets\Forsendelse;


class EfakturaOppdrag extends AbstractOppdrag
{
    public $tjeneste = 42;

    public $oppdragstype = 03;

    public $referanseFakturautsteder = '';

    public $transaksjoner = [];

    public $sumBeløp = 0;

    public $førsteForfall;

    public $sisteForfall;
}