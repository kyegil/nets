<?php
/**
 * * Part of kyegil/nets
 * Created by Kyegil
 * Date: 15/12/2020
 * Time: 10:14
 */

namespace Kyegil\Nets\Forsendelse;


class AbstractOppdrag extends \Kyegil\Nets\AbstractModel
{
    public $tjeneste;

    public $oppdragstype;

    public $oppdragsnr;

    public $oppdragskonto;

    public $transaksjoner;

    public $antallTransaksjoner;

    public $antallRecords;
    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return implode("\n", $this->records);
    }
}