<?php

namespace Kyegil\Nets;


use Exception;
use stdClass;

abstract class AbstractModel
{
    /**
     * All records
     *
     * @var array
     */
    public $records = [];

    /**
     * Start Record
     *
     * @var string
     */
    public $startrecord;

    /**
     * End Record
     *
     * @var string
     */
    public $sluttrecord;

    /**
     * Service
     *
     * @var int
     */
    public $tjeneste;

    /**
     * Når objektet omdannes til streng vises records-strengene
     *
     * @return int|string
     */
    abstract public function __toString();

    /**
     * Formatering av numeriske verdier, høyrejustert
     *
     * @param float $string
     * @param int $length
     * @param string $filler
     * @return string Formatert verdi
     */
    public function numFix($string, $length, $filler = "0" ) {
        return $this->pad($string, $length, $filler, STR_PAD_LEFT);
    }

    /**
     * Formatering av strengverdier, venstrejustert
     *
     * @param string $string
     * @param int $length
     * @param string $filler
     * @return string Formatert verdi
     */
    public function strFix($string, $length, $filler = " " ) {
        return $this->pad($string, $length, $filler);
    }

    /**
     * Pad a multibyte string to a fixed length
     *
     * @param string $string
     * @param int $length
     * @param string $filler One character
     * @param int $type STR_PAD_LEFT for right alignement or STR_PAD_RIGHT for left alignment
     * @return string
     */
    public function pad(string $string = null, int $length, $filler = " ", int $type = STR_PAD_RIGHT ): string {
        $string = mb_substr( $string, 0, $length, 'UTF-8' );
        $filler = mb_substr( $filler, 0, 1, 'UTF-8' );
        if( $type == STR_PAD_LEFT) {
            return str_repeat($filler, $length - mb_strlen($string, 'UTF-8') ) . $string;
        }
        else {
            return $string . str_repeat($filler, $length - mb_strlen($string, 'UTF-8') );
        }
    }
}